/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Bank;

/**
 *
 * @author Avee Arora
 */
public class BankAssets {
    
    private float TotalValue=100000;
    private float liquidFund=100000;
    private float FixedDeposit;
    private float CapitalMarket;
    private float loanGiven;
    private float creditCardFundUsed;
    
   private BankEquityHolding bankEquityHolding;

    public BankEquityHolding getBankEquityHolding() {
        return bankEquityHolding;
    }

    public void setBankEquityHolding(BankEquityHolding bankEquityHolding) {
        this.bankEquityHolding = bankEquityHolding;
    }
   

    public float getTotalValue() {
        return TotalValue;
    }

    public void setTotalValue(float TotalValue) {
        this.TotalValue = TotalValue;
    }

    public float getLiquidFund() {
        return liquidFund;
    }

    public void setLiquidFund(float liquidFund) {
        this.liquidFund = liquidFund;
    }

    public float getFixedDeposit() {
        return FixedDeposit;
    }

    public void setFixedDeposit(float FixedDeposit) {
        this.FixedDeposit = FixedDeposit;
    }

    public float getCapitalMarket() {
        return CapitalMarket;
    }

    public void setCapitalMarket(float CapitalMarket) {
        this.CapitalMarket = CapitalMarket;
    }

    public float getLoanGiven() {
        return loanGiven;
    }

    public void setLoanGiven(float loanGiven) {
        this.loanGiven = loanGiven;
    }

    public float getCreditCardFundUsed() {
        return creditCardFundUsed;
    }

    public void setCreditCardFundUsed(float creditCardFundUsed) {
        this.creditCardFundUsed = creditCardFundUsed;
    }
    
    
    
    
    
    
    
    
}
