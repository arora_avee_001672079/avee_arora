/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Bank;

import Business.Bank.BankEquityHolding;
import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class BankEquityHoldingList {
    
        private ArrayList<BankEquityHolding> bankEquityHoldingList;
    
    public BankEquityHoldingList()
    {
        bankEquityHoldingList= new ArrayList<>();
    }

    public ArrayList<BankEquityHolding> getBankEquityHolding() {
        return bankEquityHoldingList;
    }

    public void setBankEquityHolding(ArrayList<BankEquityHolding> bankEquityHoldingList) {
        this.bankEquityHoldingList = bankEquityHoldingList;
    }

    
    
     public BankEquityHolding addScript()
    {
        BankEquityHolding a = new BankEquityHolding();
        bankEquityHoldingList.add(a);
        return a;
    }
    
      public BankEquityHolding searchScript(String Code) {
        for (BankEquityHolding p : bankEquityHoldingList) {
            if (p.getCompanyCode().equals(Code)) {
                return p;   
            }
        }
        return null;
    }
      public void deleteScript(BankEquityHolding a )
    {
        bankEquityHoldingList.remove(a);
    }
  
    
}
