/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Bank;

/**
 *
 * @author Avee Arora
 */
public class BankRates {
    
    private float FixedDepositRate;
    private float EquityBrokrageRate;

    public float getFixedDepositRate() {
        return FixedDepositRate;
    }

    public void setFixedDepositRate(float FixedDepositRate) {
        this.FixedDepositRate = FixedDepositRate;
    }

    public float getEquityBrokrageRate() {
        return EquityBrokrageRate;
    }

    public void setEquityBrokrageRate(float EquityBrokrageRate) {
        this.EquityBrokrageRate = EquityBrokrageRate;
    }
    
    
}
