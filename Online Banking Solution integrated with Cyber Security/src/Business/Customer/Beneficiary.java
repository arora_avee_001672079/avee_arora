/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class Beneficiary {
    
    private String Name;
    private String Email;
    private int OTP;
    private Date TimeStamp;
    private boolean OTPUsed;
    private String Status;
    private int OTPWrong;

    public int getOTPWrong() {
        return OTPWrong;
    }

    public void setOTPWrong(int OTPWrong) {
        this.OTPWrong = OTPWrong;
    }
    
    
    

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    
    public boolean isOTPUsed() {
        return OTPUsed;
    }

    public void setOTPUsed(boolean OTPUsed) {
        this.OTPUsed = OTPUsed;
    }
    

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getOTP() {
        return OTP;
    }

    public void setOTP(int OTP) {
        this.OTP = OTP;
    }

    public Date getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(Date TimeStamp) {
        this.TimeStamp = TimeStamp;
    }
    
    
     @Override
    public String toString() {
        return Name;
    }
    
}
