/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class BeneficiaryList {
    
    private ArrayList<Beneficiary> beneficiaryList;
    
    public BeneficiaryList()
            
    {
        beneficiaryList= new ArrayList<>();
    }

    public ArrayList<Beneficiary> getBeneficiaryList() {
        return beneficiaryList;
    }

    public void setBeneficiaryList(ArrayList<Beneficiary> beneficiaryList) {
        this.beneficiaryList = beneficiaryList;
    }
    
    
 public Beneficiary addBeneficiary()
    {
        Beneficiary a = new Beneficiary();
        beneficiaryList.add(a);
        return a;
    }
    public void deleteBeneficiary(Beneficiary a )
    {
        beneficiaryList.remove(a);
    }
    
    public Beneficiary searchPerson(String personName)
    {
        for(Beneficiary p : beneficiaryList)
        {
            if(p.getName().equals(personName))
            {
                return p;
            }
        }
        return null;
    }
   
    
}
