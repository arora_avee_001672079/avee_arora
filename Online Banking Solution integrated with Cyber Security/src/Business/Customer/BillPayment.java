/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class BillPayment {
    
    private String PaymentName;
    private String CompanyName;

    public String getPaymentName() {
        return PaymentName;
    }

    public void setPaymentName(String PaymentName) {
        this.PaymentName = PaymentName;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }
    
    
    
    
}
