/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class BillPaymentList {
    
    
    private ArrayList<BillPayment> billPaymentList;
    
    public BillPaymentList()
    {
        billPaymentList= new ArrayList<>();
    }

    public ArrayList<BillPayment> getBillPaymentList() {
        return billPaymentList;
    }

    public void setBillPaymentList(ArrayList<BillPayment> billPaymentList) {
        this.billPaymentList = billPaymentList;
    }

  
    
    public BillPayment addPayment()
    {
        BillPayment a = new BillPayment();
        billPaymentList.add(a);
        return a;
    }
    public void deletePayment(BillPayment a )
    {
        billPaymentList.remove(a);
    }
    
    
}
