/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CapitalCompanyDataList {
    
    
    private ArrayList<CapitalCompanyData> companyList;
    
    public CapitalCompanyDataList()
    {
        companyList= new ArrayList<>();
    }

    public ArrayList<CapitalCompanyData> getCapitalCompanyDataList() {
        return companyList;
    }

    public void setCapitalCompanyDataList(ArrayList<CapitalCompanyData> companyList) {
        this.companyList = companyList;
    }
    
    public CapitalCompanyData addCompany()
    {
        CapitalCompanyData a = new CapitalCompanyData();
        companyList.add(a);
        return a;
    }
    public void deleteCompany(CapitalCompanyData a )
    {
        companyList.remove(a);
    }
    
    public CapitalCompanyData searchCode(String companyName)
    {
        for(CapitalCompanyData p : companyList)
        {
            if(p.getCompanyName().equals(companyName))
            {
                return p;
            }
        }
        return null;
    }
    
}
