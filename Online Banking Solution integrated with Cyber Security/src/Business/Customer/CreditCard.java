/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class CreditCard {
    
    String CardName;
    int CreditLimt;
    int AnnaulFee;
    float ROI;
    int CardNumber;
    private static int count = 10;
    
    public CreditCard()
    {
        count++;
        CardNumber=count;
    }

    public String getCardName() {
        return CardName;
    }

    public void setCardName(String CardName) {
        this.CardName = CardName;
    }

    public int getCreditLimt() {
        return CreditLimt;
    }

    public void setCreditLimt(int CreditLimt) {
        this.CreditLimt = CreditLimt;
    }

    public int getAnnaulFee() {
        return AnnaulFee;
    }

    public void setAnnaulFee(int AnnaulFee) {
        this.AnnaulFee = AnnaulFee;
    }

    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }

    public int getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(int CardNumber) {
        this.CardNumber = CardNumber;
    }
    
     @Override
    public String toString() {
        return CardName;
    }
    
}
