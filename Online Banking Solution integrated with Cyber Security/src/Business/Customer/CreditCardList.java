/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CreditCardList {
    
    private ArrayList<CreditCard> creditCardList;
    
    public CreditCardList()
    {
        creditCardList= new ArrayList<>();
    }

    public ArrayList<CreditCard> getCreditCardList() {
        return creditCardList;
    }

    public void setCreditCardList(ArrayList<CreditCard> creditCardList) {
        this.creditCardList = creditCardList;
    }
    
    public CreditCard addCard()
    {
        CreditCard a = new CreditCard();
        creditCardList.add(a);
        return a;
    }
    public void deleteCard(CreditCard a )
    {
        creditCardList.remove(a);
    }
    
    public CreditCard searchCard(String cardName)
    {
        for(CreditCard p : creditCardList)
        {
            if(p.CardName.equals(cardName))
            {
                return p;
            }
        }
        return null;
    }
    
}
