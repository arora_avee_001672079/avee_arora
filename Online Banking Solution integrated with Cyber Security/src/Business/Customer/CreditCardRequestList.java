/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CreditCardRequestList {
    
    
    private ArrayList<CreditCardRequest> creditCardRequest;
    
    public CreditCardRequestList()
    {
        creditCardRequest= new ArrayList<>();
    }

    public ArrayList<CreditCardRequest> getCreditCardRequest() {
        return creditCardRequest;
    }

    public void setCreditCardRequest(ArrayList<CreditCardRequest> creditCardRequest) {
        this.creditCardRequest = creditCardRequest;
    }
    
    
    public CreditCardRequest addRequest()
    {
        CreditCardRequest a = new CreditCardRequest();
        creditCardRequest.add(a);
        return a;
    }
    public void deletePerson(CreditCardRequest a )
    {
        creditCardRequest.remove(a);
    }
    
    public CreditCardRequest searchRequest(int AccountNumber)
    {
        for(CreditCardRequest p : creditCardRequest)
        {
            if(p.getAccountNumber()==AccountNumber)
            {
                return p;
            }
        }
        return null;
    }
    
    
}
