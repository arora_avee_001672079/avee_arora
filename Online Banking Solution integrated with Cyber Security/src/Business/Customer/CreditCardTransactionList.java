/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CreditCardTransactionList {
    
     private ArrayList<CreditCardTransactions> creditCardTransactions;
    
    public CreditCardTransactionList()
    {
        creditCardTransactions= new ArrayList<>();
    }

    public ArrayList<CreditCardTransactions> getCreditCardTransactions() {
        return creditCardTransactions;
    }

    public void setCreditCardTransactions(ArrayList<CreditCardTransactions> creditCardTransactions) {
        this.creditCardTransactions = creditCardTransactions;
    }

    
    
     public CreditCardTransactions addTransaction()
    {
        CreditCardTransactions a = new CreditCardTransactions();
        creditCardTransactions.add(a);
        return a;
    }
    
      public CreditCardTransactions searchTransaction(int TransactionID) {
        for (CreditCardTransactions p : creditCardTransactions) {
            if (p.getTransactionNumber()==TransactionID) {
                return p;   
            }
        }
        return null;
    }
    
}
