/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class CreditCardTransactions {
    
    
    private int TransactionNumber;
    private String TransactionType;
    private Date TransactionTime;
    
    private String Message;
    private float TransactionAmount;
    private String Status;
    private String merchant;
    private long cardNumber;

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }
    

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }
    
    
    
    private static int count =100;
    
    public CreditCardTransactions()
    {
        count++;
        TransactionNumber=count;
    }

    public int getTransactionNumber() {
        return TransactionNumber;
    }

    public void setTransactionNumber(int TransactionNumber) {
        this.TransactionNumber = TransactionNumber;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String TransactionType) {
        this.TransactionType = TransactionType;
    }

    public Date getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(Date TransactionTime) {
        this.TransactionTime = TransactionTime;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public float getTransactionAmount() {
        return TransactionAmount;
    }

    public void setTransactionAmount(float TransactionAmount) {
        this.TransactionAmount = TransactionAmount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
    
    
    
}
