/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import Business.Role.Role;
import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class Customer {
    private static int count=10000000;
    private int AccountNumber;

    public int getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(int AccountNumber) {
        this.AccountNumber = AccountNumber;
    }
    private String Email;
    private int OTP;
    private Date timestamp;
    private boolean firstTimeLogin;
    private String FirstName;
    private String LastName;
    private String address;
    private String sex;
    private long phoneNumber;
    private int SSN ;
    private Role role;
    private String UserName;
    private String Password;
    private boolean OTPUsed;
    private Date OTPTimestamp;
    private  int incorrectLoginTry;
    private String AccountStatus;
    private String IPAddress;
    private int loginInDay;
    private Date LastLogin;
    private String SignPath;
    private String Salt;
    private String DOB;
    private String Q1;
    private String Q2;
    private String Ans1 , Ans2;

    public String getQ1() {
        return Q1;
    }

    public void setQ1(String Q1) {
        this.Q1 = Q1;
    }

    public String getQ2() {
        return Q2;
    }

    public void setQ2(String Q2) {
        this.Q2 = Q2;
    }

    public String getAns1() {
        return Ans1;
    }

    public void setAns1(String Ans1) {
        this.Ans1 = Ans1;
    }

    public String getAns2() {
        return Ans2;
    }

    public void setAns2(String Ans2) {
        this.Ans2 = Ans2;
    }
    

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }
    
    

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String Salt) {
        this.Salt = Salt;
    }
    

    public String getSignPath() {
        return SignPath;
    }

    public void setSignPath(String SignPath) {
        this.SignPath = SignPath;
    }
    

    public int getLoginInDay() {
        return loginInDay;
    }

    public void setLoginInDay(int loginInDay) {
        this.loginInDay = loginInDay;
    }


    public Date getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(Date LastLogin) {
        this.LastLogin = LastLogin;
    }
    
    

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }
    
    

    public String getAccountStatus() {
        return AccountStatus;
    }

    public void setAccountStatus(String AccountStatus) {
        this.AccountStatus = AccountStatus;
    }
    

    public int getIncorrectLoginTry() {
        return incorrectLoginTry;
    }

    public void setIncorrectLoginTry(int incorrectLoginTry) {
        this.incorrectLoginTry = incorrectLoginTry;
    }

    
    
    
    

    public Customer(){
        count++;
        AccountNumber=count;
    
}
    
    public boolean isOTPUsed() {
        return OTPUsed;
    }

    public void setOTPUsed(boolean OTPUsed) {
        this.OTPUsed = OTPUsed;
    }

    public Date getOTPTimestamp() {
        return OTPTimestamp;
    }

    public void setOTPTimestamp(Date OTPTimestamp) {
        this.OTPTimestamp = OTPTimestamp;
    }
    
    

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
    

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getOTP() {
        return OTP;
    }

    public void setOTP(int OTP) {
        this.OTP = OTP;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(boolean firstTimeLogin) {
        this.firstTimeLogin = firstTimeLogin;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }
    
    
    
    
    
}
