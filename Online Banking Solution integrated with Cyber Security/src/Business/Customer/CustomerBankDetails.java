/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class CustomerBankDetails extends Customer {
    
    private Customer customer ;
    private float TotalPortfolioBalance;
    private float CheckingAccountBalance;
   private float FixedAccountBalance;
   private float CreditCardLimt;
   private float CreditCardBalance;
   private CustomerTransactionList customerTransactionList;
   private float TotalCapitalMarketInvestment;
   private float TotalProfitLoss;
   
   private String Status;
   private FixedDepositList fixedDepositList;
   private BeneficiaryList beneficiaryList;
   private CustomerCreditCardList customerCreditCardList;
   
   private CreditCardTransactionList creditCardTransactionList;
   
   private CustomerLoanList customerLoanList;
   private CustomerCapitalMarketTransctionList customerCapitalMarketTransctionList;
   private CustomerCapitalMarketList customerCapitalMarketList;

    public CreditCardTransactionList getCreditCardTransactionList() {
        return creditCardTransactionList;
    }

    public void setCreditCardTransactionList(CreditCardTransactionList creditCardTransactionList) {
        this.creditCardTransactionList = creditCardTransactionList;
    }

   
   
    public float getTotalProfitLoss() {
        return TotalProfitLoss;
    }

    public void setTotalProfitLoss(float TotalProfitLoss) {
        this.TotalProfitLoss = TotalProfitLoss;
    }

    public CustomerCapitalMarketList getCustomerCapitalMarketList() {
        return customerCapitalMarketList;
    }

    public void setCustomerCapitalMarketList(CustomerCapitalMarketList customerCapitalMarketList) {
        this.customerCapitalMarketList = customerCapitalMarketList;
    }
   
   

    public CustomerCapitalMarketTransctionList getCustomerCapitalMarketTransctionList() {
        return customerCapitalMarketTransctionList;
    }

    public void setCustomerCapitalMarketTransctionList(CustomerCapitalMarketTransctionList customerCapitalMarketTransctionList) {
        this.customerCapitalMarketTransctionList = customerCapitalMarketTransctionList;
    }

   
   

    public CustomerLoanList getCustomerLoanList() {
        return customerLoanList;
    }

    public void setCustomerLoanList(CustomerLoanList customerLoanList) {
        this.customerLoanList = customerLoanList;
    }
   
   

    public CustomerCreditCardList getCustomerCreditCardList() {
        return customerCreditCardList;
    }

    public void setCustomerCreditCardList(CustomerCreditCardList customerCreditCardList) {
        this.customerCreditCardList = customerCreditCardList;
    }
   
   
   

    public BeneficiaryList getBeneficiaryList() {
        return beneficiaryList;
    }

    public void setBeneficiaryList(BeneficiaryList beneficiaryList) {
        this.beneficiaryList = beneficiaryList;
    }

    public FixedDepositList getFixedDepositList() {
        return fixedDepositList;
    }

    public void setFixedDepositList(FixedDepositList fixedDepositList) {
        this.fixedDepositList = fixedDepositList;
    }
   
   

   public CustomerBankDetails()
   {
       customerTransactionList= new CustomerTransactionList();
       fixedDepositList= new FixedDepositList();
       beneficiaryList= new BeneficiaryList();
       customerCreditCardList = new CustomerCreditCardList();
       customerLoanList= new CustomerLoanList();
       customerCapitalMarketTransctionList = new CustomerCapitalMarketTransctionList();
       customerCapitalMarketList = new CustomerCapitalMarketList();
       creditCardTransactionList = new CreditCardTransactionList();
       
   }
    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
   

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public float getTotalPortfolioBalance() {
        return TotalPortfolioBalance;
    }

    public void setTotalPortfolioBalance(float TotalPortfolioBalance) {
        this.TotalPortfolioBalance = TotalPortfolioBalance;
    }

    public float getCheckingAccountBalance() {
        return CheckingAccountBalance;
    }

    public void setCheckingAccountBalance(float CheckingAccountBalance) {
        this.CheckingAccountBalance = CheckingAccountBalance;
    }

    public float getFixedAccountBalance() {
        return FixedAccountBalance;
    }

    public void setFixedAccountBalance(float FixedAccountBalance) {
        this.FixedAccountBalance = FixedAccountBalance;
    }

    public float getCreditCardLimt() {
        return CreditCardLimt;
    }

    public void setCreditCardLimt(float CreditCardLimt) {
        this.CreditCardLimt = CreditCardLimt;
    }

    public float getCreditCardBalance() {
        return CreditCardBalance;
    }

    public void setCreditCardBalance(float CreditCardBalance) {
        this.CreditCardBalance = CreditCardBalance;
    }

    public CustomerTransactionList getCustomerTransactionList() {
        return customerTransactionList;
    }

    public void setCustomerTransactionList(CustomerTransactionList customerTransactionList) {
        this.customerTransactionList = customerTransactionList;
    }

    public float getTotalCapitalMarketInvestment() {
        return TotalCapitalMarketInvestment;
    }

    public void setTotalCapitalMarketInvestment(float TotalCapitalMarketInvestment) {
        this.TotalCapitalMarketInvestment = TotalCapitalMarketInvestment;
    }
   
   
    
}
