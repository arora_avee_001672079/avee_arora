/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerBankDetailsList {
    
    private ArrayList<CustomerBankDetails> customerBankDetails ;
    
    public CustomerBankDetailsList()
    {
        customerBankDetails= new ArrayList<>();
    }

    public ArrayList<CustomerBankDetails> getCustomerBankDetails() {
        return customerBankDetails;
    }

    public void setCustomerBankDetails(ArrayList<CustomerBankDetails> customerBankDetails) {
        this.customerBankDetails = customerBankDetails;
    }
    
     public CustomerBankDetails addBankDetails()
    {
        CustomerBankDetails a = new CustomerBankDetails();
        customerBankDetails.add(a);
        return a;
    }
     
     public CustomerBankDetails searchCustomer(String Email) {
        for (CustomerBankDetails p : customerBankDetails) {
            if (p.getCustomer().getEmail().equalsIgnoreCase(Email)) {
                return p;   
            }
        }
        return null;
    }
  
    
    
}
