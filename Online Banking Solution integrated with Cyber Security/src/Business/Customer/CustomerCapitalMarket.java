/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class CustomerCapitalMarket {
    
    private String CompanyName;
    private String CompanyCode;
    private int Quantity;
    private String Sector;
    private float AveragePrice;

    public float getAveragePrice() {
        return AveragePrice;
    }

    public void setAveragePrice(float AveragePrice) {
        this.AveragePrice = AveragePrice;
    }
    



    
    

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String CompanyCode) {
        this.CompanyCode = CompanyCode;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public String getSector() {
        return Sector;
    }

    public void setSector(String Sector) {
        this.Sector = Sector;
    }
    
    
    
}
