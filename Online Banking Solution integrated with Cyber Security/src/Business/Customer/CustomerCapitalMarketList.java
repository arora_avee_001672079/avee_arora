/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerCapitalMarketList {
    
      private ArrayList<CustomerCapitalMarket> customerCapitalMarketList;
    
    public CustomerCapitalMarketList()
    {
        customerCapitalMarketList= new ArrayList<>();
    }

    public ArrayList<CustomerCapitalMarket> getCustomerCapitalMarket() {
        return customerCapitalMarketList;
    }

    public void setCustomerCapitalMarket(ArrayList<CustomerCapitalMarket> customerCapitalMarketList) {
        this.customerCapitalMarketList = customerCapitalMarketList;
    }

    
    
     public CustomerCapitalMarket addScript()
    {
        CustomerCapitalMarket a = new CustomerCapitalMarket();
        customerCapitalMarketList.add(a);
        return a;
    }
    
      public CustomerCapitalMarket searchScript(String Code) {
        for (CustomerCapitalMarket p : customerCapitalMarketList) {
            if (p.getCompanyCode().equals(Code)) {
                return p;   
            }
        }
        return null;
    }
      public void deleteScript(CustomerCapitalMarket a )
    {
        customerCapitalMarketList.remove(a);
    }
    
   
    
}
