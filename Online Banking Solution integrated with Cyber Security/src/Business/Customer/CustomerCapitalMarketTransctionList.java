/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerCapitalMarketTransctionList {
    
      private ArrayList<CustomerCapitalMarketTransaction> customerCapitalMarketTransactionList;
    
    public CustomerCapitalMarketTransctionList()
    {
        customerCapitalMarketTransactionList= new ArrayList<>();
    }

    public ArrayList<CustomerCapitalMarketTransaction> getCustomerCapitalMarket() {
        return customerCapitalMarketTransactionList;
    }

    public void setCustomerCapitalMarket(ArrayList<CustomerCapitalMarketTransaction> customerCapitalMarketTransactionList) {
        this.customerCapitalMarketTransactionList = customerCapitalMarketTransactionList;
    }

    
    
     public CustomerCapitalMarketTransaction addTransaction()
    {
        CustomerCapitalMarketTransaction a = new CustomerCapitalMarketTransaction();
        customerCapitalMarketTransactionList.add(a);
        return a;
    }
    
      public CustomerCapitalMarketTransaction searchTransaction(int Code) {
        for (CustomerCapitalMarketTransaction p : customerCapitalMarketTransactionList) {
            if (p.getTranactionNumber()==Code) {
                return p;   
            }
        }
        return null;
    }
   
    
}
