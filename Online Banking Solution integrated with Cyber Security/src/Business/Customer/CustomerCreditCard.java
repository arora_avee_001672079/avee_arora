/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class CustomerCreditCard {
    
    private String CardType;
    private float ROI;
    private float Limit;
    private Date activateDate;
    private CreditCardTransactionList creditCardTransactionList;
    private float MinimumDue ;
    private long creditCardNumber;
    private int cvv ;
    private String ddmm;

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getDdmm() {
        return ddmm;
    }

    public void setDdmm(String ddmm) {
        this.ddmm = ddmm;
    }
    
    
    
   // private static long count=1111111111111111L ;
    

    public long getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(long creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }
    
//    public CustomerCreditCard()
//    {
//        count++;
//        creditCardNumber=count;
//    }

    public float getMinimumDue() {
        return MinimumDue;
    }

    public void setMinimumDue(float MinimumDue) {
        this.MinimumDue = MinimumDue;
    }
    
    

    public CreditCardTransactionList getCreditCardTransactionList() {
        return creditCardTransactionList;
    }

    public void setCreditCardTransactionList(CreditCardTransactionList creditCardTransactionList) {
        this.creditCardTransactionList = creditCardTransactionList;
    }

    
    public String getCardType() {
        return CardType;
    }

    public void setCardType(String CardType) {
        this.CardType = CardType;
    }

    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }

    public float getLimit() {
        return Limit;
    }

    public void setLimit(float Limit) {
        this.Limit = Limit;
    }

    public Date getActivateDate() {
        return activateDate;
    }

    public void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }
    
    
     
    
}
