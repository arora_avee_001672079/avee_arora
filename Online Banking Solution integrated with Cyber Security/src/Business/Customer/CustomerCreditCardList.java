/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerCreditCardList {
    
    
      private ArrayList<CustomerCreditCard> customerCreditCards;
    
    public CustomerCreditCardList()
    {
        customerCreditCards= new ArrayList<>();
    }

    public ArrayList<CustomerCreditCard> getCustomerCreditCard() {
        return customerCreditCards;
    }

    public void setCustomerCreditCard(ArrayList<CustomerCreditCard> customerCreditCards) {
        this.customerCreditCards = customerCreditCards;
    }

    
    
     public CustomerCreditCard addCard()
    {
        CustomerCreditCard a = new CustomerCreditCard();
        customerCreditCards.add(a);
        return a;
    }
    
      public CustomerCreditCard searchCard(String CardType) {
        for (CustomerCreditCard p : customerCreditCards) {
            if (p.getCardType()==CardType) {
                return p;   
            }
        }
        return null;
    }
      
         public CustomerCreditCard Authenticate(long card,int cvv) {
        for (CustomerCreditCard p : customerCreditCards) {
            if (p.getCreditCardNumber()==card && p.getCvv()==cvv) {
                return p;   
            }
        }
        return null;
    }
   
    
    
}
