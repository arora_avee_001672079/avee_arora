/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerDirectory {
    
    private ArrayList<Customer> newCustomerDirectory;

    public ArrayList<Customer> getNewCustomerDirectory() {
        return newCustomerDirectory;
    }

    public void setNewCustomerDirectory(ArrayList<Customer> newCustomerDirectory) {
        this.newCustomerDirectory = newCustomerDirectory;
    }
    
    public CustomerDirectory()
    {
        newCustomerDirectory=new ArrayList<>();
    }
//    
//     public Customer addCustomer() {
//        Customer a = new Customer();
//        newCustomerDirectory.add(a);
//        return a;
//    }

     public Customer createCustomerAccount(){
        Customer customer = new Customer();
               newCustomerDirectory.add(customer);
        
        return customer;
    }

    public Customer searchCustomer(String userName ) {
        for (Customer p : newCustomerDirectory) {
            if (p.getUserName().equalsIgnoreCase(userName) ) {
                return p;   
            }
        }
        return null;
    }

    
}
