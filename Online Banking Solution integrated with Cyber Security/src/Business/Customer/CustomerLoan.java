/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class CustomerLoan {
    
    private String LoanType;
    private float ROI;
    private float Amount;
    private float TimePeriod;
    private float MonthlyInstalment ;
    private Date StartDate ;
    private int LoanAccountNumber;
    private float EndDate;
    private String Status;
    private static int count=10000;
    private CustomerLoanTransactionList customerLoanTransactionList;

    public CustomerLoanTransactionList getCustomerLoanTransactionList() {
        return customerLoanTransactionList;
    }

    public void setCustomerLoanTransactionList(CustomerLoanTransactionList customerLoanTransactionList) {
        this.customerLoanTransactionList = customerLoanTransactionList;
    }
    
   
    
    
    public CustomerLoan()
    {
        count++;
        LoanAccountNumber=count;
        customerLoanTransactionList= new CustomerLoanTransactionList();
    }

    public String getLoanType() {
        return LoanType;
    }

    public void setLoanType(String LoanType) {
        this.LoanType = LoanType;
    }

    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }

    public float getAmount() {
        return Amount;
    }

    public void setAmount(float Amount) {
        this.Amount = Amount;
    }

    public float getTimePeriod() {
        return TimePeriod;
    }

    public void setTimePeriod(float TimePeriod) {
        this.TimePeriod = TimePeriod;
    }

    public float getMonthlyInstalment() {
        return MonthlyInstalment;
    }

    public void setMonthlyInstalment(float MonthlyInstalment) {
        this.MonthlyInstalment = MonthlyInstalment;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date StartDate) {
        this.StartDate = StartDate;
    }

    public int getLoanAccountNumber() {
        return LoanAccountNumber;
    }

    public void setLoanAccountNumber(int LoanAccountNumber) {
        this.LoanAccountNumber = LoanAccountNumber;
    }

    public float getEndDate() {
        return EndDate;
    }

    public void setEndDate(float EndDate) {
        this.EndDate = EndDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
    
      @Override
    public String toString() {
        return (String.valueOf(LoanAccountNumber)); //To change body of generated methods, choose Tools | Templates.
    }
    
}
