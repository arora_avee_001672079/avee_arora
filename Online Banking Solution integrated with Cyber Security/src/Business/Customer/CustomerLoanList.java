/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerLoanList {
    
      private ArrayList<CustomerLoan> customerLoanList;
    
    public CustomerLoanList()
    {
        customerLoanList= new ArrayList<>();
    }

    public ArrayList<CustomerLoan> getCustomerLoanList() {
        return customerLoanList;
    }

    public void setCustomerLoanList(ArrayList<CustomerLoan> customerLoanList) {
        this.customerLoanList = customerLoanList;
    }
    
    public CustomerLoan addLoan()
    {
        CustomerLoan a = new CustomerLoan();
        customerLoanList.add(a);
        return a;
    }
    public void deleteLoan(CustomerLoan a )
    {
        customerLoanList.remove(a);
    }
    
    public CustomerLoan searchCustomerLoan(int AccountNumber)
    {
        for(CustomerLoan p : customerLoanList)
        {
            if(p.getLoanAccountNumber()==AccountNumber)
            {
                return p;
            }
        }
        return null;
    }
    
}
