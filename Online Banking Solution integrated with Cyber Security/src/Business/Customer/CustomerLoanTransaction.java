/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class CustomerLoanTransaction {
    
    private int TranacationNumber;
    private Date timeStampTransaction;
    private float DuePayment;
    private float Principal;
    private float interest;
    private float TotalInterest;
    private float Balance;
    private int LoanNumber;
    private String status;
    private Date dueDate;

    public int getTranacationNumber() {
        return TranacationNumber;
    }

    public void setTranacationNumber(int TranacationNumber) {
        this.TranacationNumber = TranacationNumber;
    }

    public Date getTimeStampTransaction() {
        return timeStampTransaction;
    }

    public void setTimeStampTransaction(Date timeStampTransaction) {
        this.timeStampTransaction = timeStampTransaction;
    }

    public float getDuePayment() {
        return DuePayment;
    }

    public void setDuePayment(float DuePayment) {
        this.DuePayment = DuePayment;
    }

    public float getPrincipal() {
        return Principal;
    }

    public void setPrincipal(float Principal) {
        this.Principal = Principal;
    }

    public float getInterest() {
        return interest;
    }

    public void setInterest(float interest) {
        this.interest = interest;
    }

    public float getTotalInterest() {
        return TotalInterest;
    }

    public void setTotalInterest(float TotalInterest) {
        this.TotalInterest = TotalInterest;
    }

    public float getBalance() {
        return Balance;
    }

    public void setBalance(float Balance) {
        this.Balance = Balance;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    
    public int getLoanNumber() {
        return LoanNumber;
    }

    public void setLoanNumber(int LoanNumber) {
        this.LoanNumber = LoanNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
     @Override
    public String toString() {
        return dueDate.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
