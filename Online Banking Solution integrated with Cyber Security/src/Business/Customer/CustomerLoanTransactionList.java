/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerLoanTransactionList {
    
     private ArrayList<CustomerLoanTransaction> CustomerLoanTransaction;
    
    public CustomerLoanTransactionList()
    {
        CustomerLoanTransaction= new ArrayList<>();
    }

    public ArrayList<CustomerLoanTransaction> getCustomerLoanTransactionTransactionList() {
        return CustomerLoanTransaction;
    }

    public void setCustomerLoanTransactionTransactionList(ArrayList<CustomerLoanTransaction> CustomerLoanTransaction) {
        this.CustomerLoanTransaction = CustomerLoanTransaction;
    }
    
    public CustomerLoanTransaction addLoanTransaction()
    {
        CustomerLoanTransaction a = new CustomerLoanTransaction();
        CustomerLoanTransaction.add(a);
        return a;
    }
    public void deleteTransaction(CustomerLoanTransaction a )
    {
        CustomerLoanTransaction.remove(a);
    }
    
    public CustomerLoanTransaction searchCustomerLoanTransaction(int AccountNumber)
    {
        for(CustomerLoanTransaction p : CustomerLoanTransaction)
        {
            if(p.getLoanNumber()==AccountNumber)
            {
                return p;
            }
        }
        return null;
    }
    
}
