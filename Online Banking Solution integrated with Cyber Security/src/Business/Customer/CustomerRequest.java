/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class CustomerRequest {
    
    private String FirstName;
    private String LastName;
    private int AccountNumber;
    private String CardType;
    private String Status;
private float MonthlyIncome;
private float cardAssignedLimit;
private String email;
private float ROI ;
private String RequestType;
private int Tenure;
private float RequestedAmount;

    public float getRequestedAmount() {
        return RequestedAmount;
    }

    public void setRequestedAmount(float RequestedAmount) {
        this.RequestedAmount = RequestedAmount;
    }



    public int getTenure() {
        return Tenure;
    }

    public void setTenure(int Tenure) {
        this.Tenure = Tenure;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        CustomerRequest.count = count;
    }


    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String RequestType) {
        this.RequestType = RequestType;
    }


    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public float getCardAssignedLimit() {
        return cardAssignedLimit;
    }

    public void setCardAssignedLimit(float cardAssignedLimit) {
        this.cardAssignedLimit = cardAssignedLimit;
    }


    public float getMonthlyIncome() {
        return MonthlyIncome;
    }

    public void setMonthlyIncome(float MonthlyIncome) {
        this.MonthlyIncome = MonthlyIncome;
    }

    public int getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(int requestNumber) {
        this.requestNumber = requestNumber;
    }
private float MonthlyExpenditure;

private int requestNumber;
private static int count=1000; 

public CustomerRequest()
{
    count++;
    requestNumber=count;
}

    public float getMonthlyExpenditure() {
        return MonthlyExpenditure;
    }

    public void setMonthlyExpenditure(float MonthlyExpenditure) {
        this.MonthlyExpenditure = MonthlyExpenditure;
    }



    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public int getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(int AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String CardType) {
        this.CardType = CardType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
    
    
}
