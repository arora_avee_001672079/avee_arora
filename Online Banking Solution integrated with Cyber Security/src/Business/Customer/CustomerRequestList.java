/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerRequestList {
    
    
    private ArrayList<CustomerRequest> creditCardRequest;
    
    public CustomerRequestList()
    {
        creditCardRequest= new ArrayList<>();
    }

    public ArrayList<CustomerRequest> getCreditCardRequest() {
        return creditCardRequest;
    }

    public void setCreditCardRequest(ArrayList<CustomerRequest> creditCardRequest) {
        this.creditCardRequest = creditCardRequest;
    }
    
    
    public CustomerRequest addRequest()
    {
        CustomerRequest a = new CustomerRequest();
        creditCardRequest.add(a);
        return a;
    }
    public void deletePerson(CustomerRequest a )
    {
        creditCardRequest.remove(a);
    }
    
    public CustomerRequest searchRequest(int AccountNumber)
    {
        for(CustomerRequest p : creditCardRequest)
        {
            if(p.getAccountNumber()==AccountNumber)
            {
                return p;
            }
        }
        return null;
    }
    
    
}
