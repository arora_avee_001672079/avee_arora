/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import Business.Utilities.GenerateUniqueNumberClass;
import java.util.Date;
import org.joda.time.DateTime;

/**
 *
 * @author Avee Arora
 */
public class CustomerTransaction {
    private int TransactionNumber;
    private String TransactionType;
    private Date TransactionTime;
    private String TransactionTo;
    private String Message;
    private float TransactionAmount;
    private String Status;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    

    public CustomerTransaction()
    {
        GenerateUniqueNumberClass transaction=new GenerateUniqueNumberClass();
        int number=transaction.GenerateTransaction();
        TransactionNumber=number;
    }
    public int getTransactionNumber() {
        return TransactionNumber;
    }

    public void setTransactionNumber(int TransactionNumber) {
        this.TransactionNumber = TransactionNumber;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String TransactionType) {
        this.TransactionType = TransactionType;
    }

    public Date getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(Date TransactionTime) {
        this.TransactionTime = TransactionTime;
    }

  
   

    public String getTransactionTo() {
        return TransactionTo;
    }

    public void setTransactionTo(String TransactionTo) {
        this.TransactionTo = TransactionTo;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public float getTransactionAmount() {
        return TransactionAmount;
    }

    public void setTransactionAmount(float TransactionAmount) {
        this.TransactionAmount = TransactionAmount;
    }
     @Override
    public String toString() {
        return TransactionTime.toString();
    }
    
}
