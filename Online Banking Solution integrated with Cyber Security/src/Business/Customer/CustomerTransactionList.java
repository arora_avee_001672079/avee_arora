/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class CustomerTransactionList {
    
    private ArrayList<CustomerTransaction> customerTransactions;
    
    public CustomerTransactionList()
    {
        customerTransactions= new ArrayList<>();
    }

    public ArrayList<CustomerTransaction> getCustomerTransactions() {
        return customerTransactions;
    }

    public void setCustomerTransactions(ArrayList<CustomerTransaction> customerTransactions) {
        this.customerTransactions = customerTransactions;
    }
    
     public CustomerTransaction addTransaction()
    {
        CustomerTransaction a = new CustomerTransaction();
        customerTransactions.add(a);
        return a;
    }
    
      public CustomerTransaction searchTransaction(int TransactionID) {
        for (CustomerTransaction p : customerTransactions) {
            if (p.getTransactionNumber()==TransactionID) {
                return p;   
            }
        }
        return null;
    }
  
     
}
