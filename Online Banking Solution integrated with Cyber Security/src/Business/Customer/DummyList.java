/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class DummyList {

    private ArrayList<Dummy> dummyList;

    public ArrayList<Dummy> getDummyList() {
        return dummyList;
    }

    public void setDummyList(ArrayList<Dummy> dummyList) {
        this.dummyList = dummyList;
    }

    public DummyList() {
        dummyList = new ArrayList<>();
    }

    public Dummy addOrder() {
        Dummy a = new Dummy();
        dummyList.add(a);
        return a;
    }

    public void deleteOrder(Dummy a) {
        dummyList.remove(a);
    }
}
