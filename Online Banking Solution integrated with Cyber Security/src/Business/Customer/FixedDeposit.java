/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

/**
 *
 * @author Avee Arora
 */
public class FixedDeposit {
    
    private String CustomerName;
    private int CustomerAccountNumber;
    private Date startDate;
    private Date EndDate;
    private float PrincipleAmount ;
    private float MaturityAmount;
    private float Timeperiod;
    private float ROI;
    private String status ;
    private int FixedDepositNumber;
    private Date liquidateDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    public Date getLiquidateDate() {
        return liquidateDate;
    }

    public void setLiquidateDate(Date liquidateDate) {
        this.liquidateDate = liquidateDate;
    }

    

   



    public int getFixedDepositNumber() {
        return FixedDepositNumber;
    }

    public void setFixedDepositNumber(int FixedDepositNumber) {
        this.FixedDepositNumber = FixedDepositNumber;
    }
    

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public int getCustomerAccountNumber() {
        return CustomerAccountNumber;
    }

    public void setCustomerAccountNumber(int CustomerAccountNumber) {
        this.CustomerAccountNumber = CustomerAccountNumber;
    }



    public float getPrincipleAmount() {
        return PrincipleAmount;
    }

    public void setPrincipleAmount(float PrincipleAmount) {
        this.PrincipleAmount = PrincipleAmount;
    }

    public float getMaturityAmount() {
        return MaturityAmount;
    }

    public void setMaturityAmount(float MaturityAmount) {
        this.MaturityAmount = MaturityAmount;
    }

    public float getTimeperiod() {
        return Timeperiod;
    }

    public void setTimeperiod(float Timeperiod) {
        this.Timeperiod = Timeperiod;
    }

    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
     @Override
    public String toString() {
        return String.valueOf(FixedDepositNumber);
    }
    
}
