/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class FixedDepositList {
    
    private ArrayList<FixedDeposit> fixedDepositList;
    
    public FixedDepositList()
    {
        fixedDepositList= new ArrayList<>();
        
    }

    public ArrayList<FixedDeposit> getFixedDepositList() {
        return fixedDepositList;
    }

    public void setFixedDepositList(ArrayList<FixedDeposit> fixedDepositList) {
        this.fixedDepositList = fixedDepositList;
    }
    
    public FixedDeposit addFixedDeposit()
    {
        FixedDeposit a = new FixedDeposit();
        fixedDepositList.add(a);
        return a;
    }
    public void breakFixedDepoist(FixedDeposit a )
    {
        fixedDepositList.remove(a);
    }
    
    public FixedDeposit searchFixedDeposit(int fixedDepositNumber)
    {
        for(FixedDeposit p : fixedDepositList)
        {
            if(p.getFixedDepositNumber()==fixedDepositNumber)
            {
                return p;
            }
        }
        return null;
    }
    
}
