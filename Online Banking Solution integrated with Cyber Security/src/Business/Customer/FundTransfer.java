/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.Date;
import org.joda.time.DateTime;

/**
 *
 * @author Avee Arora
 */
public class FundTransfer {
    
    private String From;
    private String To;
    private int TranscationNumber;
    private float Amount;
    private String Status;
    private Date TimeStamp;

    public String getFrom() {
        return From;
    }

    public void setFrom(String From) {
        this.From = From;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String To) {
        this.To = To;
    }

    public int getTranscationNumber() {
        return TranscationNumber;
    }

    public void setTranscationNumber(int TranscationNumber) {
        this.TranscationNumber = TranscationNumber;
    }

    public float getAmount() {
        return Amount;
    }

    public void setAmount(float Amount) {
        this.Amount = Amount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public Date getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(Date TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

 


    
    
    
    
    
    
}
