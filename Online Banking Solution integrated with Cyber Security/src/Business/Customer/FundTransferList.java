/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class FundTransferList {
    
    private ArrayList<FundTransfer> fundTransfersList;
    
    public FundTransferList()
    {
        fundTransfersList= new ArrayList<>();
    }

    public ArrayList<FundTransfer> getFundTransfersList() {
        return fundTransfersList;
    }

    public void setFundTransfersList(ArrayList<FundTransfer> fundTransfersList) {
        this.fundTransfersList = fundTransfersList;
    }
    
     public FundTransfer addTransfer()
    {
        FundTransfer a = new FundTransfer();
        fundTransfersList.add(a);
        return a;
    }
    public void deletePerson(FundTransfer a )
    {
        fundTransfersList.remove(a);
    }
    
    public FundTransfer searchTrasnferTO(String Email)
    {
        for(FundTransfer p : fundTransfersList)
        {
            if(p.getTo().equals(Email))
            {
                return p;
            }
        }
        return null;
    }
    
    public FundTransfer searchTrasnferFrom(String Email)
    {
        for(FundTransfer p : fundTransfersList)
        {
            if(p.getTo().equals(Email))
            {
                return p;
            }
        }
        return null;
    }
    
}
