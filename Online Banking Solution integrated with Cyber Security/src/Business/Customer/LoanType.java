/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

/**
 *
 * @author Avee Arora
 */
public class LoanType {
    
    private String LoanType;
    private float ROI;
    private int MaximumPeriod;
    private int MinimumPeriod;
    private int LoanUniqueNumber;
    private static int count=0;

    public int getLoanUniqueNumber() {
        return LoanUniqueNumber;
    }

    public void setLoanUniqueNumber(int LoanUniqueNumber) {
        this.LoanUniqueNumber = LoanUniqueNumber;
    }
    
    public LoanType()
    {
        count++;
        LoanUniqueNumber=count;
    }

    public String getLoanType() {
        return LoanType;
    }

    public void setLoanType(String LoanType) {
        this.LoanType = LoanType;
    }

    public float getROI() {
        return ROI;
    }

    public void setROI(float ROI) {
        this.ROI = ROI;
    }

    public int getMaximumPeriod() {
        return MaximumPeriod;
    }

    public void setMaximumPeriod(int MaximumPeriod) {
        this.MaximumPeriod = MaximumPeriod;
    }

    public int getMinimumPeriod() {
        return MinimumPeriod;
    }

    public void setMinimumPeriod(int MinimumPeriod) {
        this.MinimumPeriod = MinimumPeriod;
    }
     @Override
    public String toString() {
        return LoanType; //To change body of generated methods, choose Tools | Templates.
    }
    
}
