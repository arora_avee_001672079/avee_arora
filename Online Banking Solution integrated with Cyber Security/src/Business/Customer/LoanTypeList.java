/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class LoanTypeList {
    
      private ArrayList<LoanType> loanTypeList;
    
    public LoanTypeList()
    {
        loanTypeList= new ArrayList<>();
    }

    public ArrayList<LoanType> getLoanTypeList() {
        return loanTypeList;
    }

    public void setLoanTypeList(ArrayList<LoanType> loanTypeList) {
        this.loanTypeList = loanTypeList;
    }
    
    public LoanType addLoan()
    {
        LoanType a = new LoanType();
        loanTypeList.add(a);
        return a;
    }
    public void deleteLoan(LoanType a )
    {
        loanTypeList.remove(a);
    }
    
    public LoanType searchLoanType(String cardName)
    {
        for(LoanType p : loanTypeList)
        {
            if(p.getLoanType().equals(cardName))
            {
                return p;
            }
        }
        return null;
    }
    
}
