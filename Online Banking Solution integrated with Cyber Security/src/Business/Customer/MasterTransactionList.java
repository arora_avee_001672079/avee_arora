/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class MasterTransactionList {
    
    private ArrayList<MasterTransaction> masterTransactionsList;
    
    public MasterTransactionList()
    {
        masterTransactionsList= new ArrayList<>();
        
    }

    public ArrayList<MasterTransaction> getMasterTransactionsList() {
        return masterTransactionsList;
    }

    public void setMasterTransactionsList(ArrayList<MasterTransaction> masterTransactionsList) {
        this.masterTransactionsList = masterTransactionsList;
    }
    
    public MasterTransaction addTransaction()
    {
        MasterTransaction a = new MasterTransaction();
        masterTransactionsList.add(a);
        return a;
    }
    
    public MasterTransaction searchPerson(int AccountNumber)
    {
        for(MasterTransaction p : masterTransactionsList)
        {
            if(p.getAccountNumber()==AccountNumber)
            {
                return p;
            }
        }
        return null;
    }
    
}
