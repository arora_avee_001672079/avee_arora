/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.CustomerRelationshipManagerRole;
import Business.Role.CustomerRole;
import Business.Role.SecurityOfficerL1;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class BankOrganization extends Organization{

    public BankOrganization() {
        super(Organization.Type.Bank.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new CustomerRelationshipManagerRole());
        roles.add(new CustomerRole());
        return roles;
    }
     
}