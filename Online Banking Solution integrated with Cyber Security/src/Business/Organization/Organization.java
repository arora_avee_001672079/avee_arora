/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Bank.BankAssets;
import Business.Bank.BankRates;
import Business.Customer.BillPaymentList;
import Business.Customer.CapitalCompanyData;
import Business.Customer.CapitalCompanyDataList;
import Business.Customer.CreditCardList;
import Business.Customer.CustomerRequestList;
import Business.Customer.CustomerBankDetails;
import Business.Customer.CustomerBankDetailsList;
import Business.Customer.CustomerDirectory;
import Business.Customer.FundTransferList;
import Business.Customer.CustomerLoanList;
import Business.Customer.LoanTypeList;
import Business.Customer.MasterTransactionList;
import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.SecurityOffice.SecurityConcernsList;
import Business.UserAccount.UserAccountDirectory;
import Business.Utilities.CompanySymbols;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private CustomerDirectory customerDirectory;
    private CustomerBankDetailsList customerBankDetails;
    private MasterTransactionList masterTransactionList;
    private FundTransferList fundTransferList;
    private CustomerRequestList creditCardRequestList;
    private CreditCardList creditCardList;
    private LoanTypeList loanTypeList;
    private BillPaymentList billPaymentList;
    private CapitalCompanyDataList companyList;
    private SecurityConcernsList securityConcernsList;
    private BankAssets bankAssets;
    private BankRates bankRates;

    public BillPaymentList getBillPaymentList() {
        return billPaymentList;
    }

    public void setBillPaymentList(BillPaymentList billPaymentList) {
        this.billPaymentList = billPaymentList;
    }

    public BankRates getBankRates() {
        return bankRates;
    }

    public void setBankRates(BankRates bankRates) {
        this.bankRates = bankRates;
    }

    public BankAssets getBankAssets() {
        return bankAssets;
    }

    public void setBankAssets(BankAssets bankAssets) {
        this.bankAssets = bankAssets;
    }
    
    

    public SecurityConcernsList getSecurityConcernsList() {
        return securityConcernsList;
    }

    public void setSecurityConcernsList(SecurityConcernsList securityConcernsList) {
        this.securityConcernsList = securityConcernsList;
    }
    

    public CapitalCompanyDataList getCompanyList() {
        return companyList;
    }

    public void setCompanyList(CapitalCompanyDataList companyList) {
        this.companyList = companyList;
    }
    
    

    public LoanTypeList getLoanTypeList() {
        return loanTypeList;
    }

    public void setLoanTypeList(LoanTypeList loanTypeList) {
        this.loanTypeList = loanTypeList;
    }
    

    public CustomerRequestList getCreditCardRequestList() {
        return creditCardRequestList;
    }

    public void setCreditCardRequestList(CustomerRequestList creditCardRequestList) {
        this.creditCardRequestList = creditCardRequestList;
    }

    public CreditCardList getCreditCardList() {
        return creditCardList;
    }

    public void setCreditCardList(CreditCardList creditCardList) {
        this.creditCardList = creditCardList;
    }
    

    public FundTransferList getFundTransferList() {
        return fundTransferList;
    }

    public void setFundTransferList(FundTransferList fundTransferList) {
        this.fundTransferList = fundTransferList;
    }
    

    public MasterTransactionList getMasterTransactionList() {
        return masterTransactionList;
    }

    public void setMasterTransactionList(MasterTransactionList masterTransactionList) {
        this.masterTransactionList = masterTransactionList;
    }

    public CustomerBankDetailsList getCustomerBankDetails() {
        return customerBankDetails;
    }

    public void setCustomerBankDetails(CustomerBankDetailsList customerBankDetails) {
        this.customerBankDetails = customerBankDetails;
    }

   

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }
    private int organizationID;
    private static int counter;
    
    public enum Type{
        Admin("Admin Organization"), Bank("Bank Organization"), Security("Security Organization");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        customerDirectory=new CustomerDirectory();
        customerBankDetails= new CustomerBankDetailsList();
        masterTransactionList= new MasterTransactionList();
        fundTransferList= new FundTransferList();
        creditCardRequestList= new CustomerRequestList();
        creditCardList=new CreditCardList();
        loanTypeList= new LoanTypeList();
        companyList=new CapitalCompanyDataList();
        securityConcernsList= new SecurityConcernsList();
        bankAssets = new BankAssets();
        bankRates=new BankRates();
        billPaymentList=new BillPaymentList();
        
             
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
