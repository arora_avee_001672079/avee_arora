/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.SecurityOfficerL1;
import Business.Role.Role;
import Business.Role.SecurityOfficerL2;
import Business.Role.SecurityOfficerL3;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class SecurityOrganisation extends Organization{

    public SecurityOrganisation() {
        super(Organization.Type.Security.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new SecurityOfficerL1());
         roles.add(new SecurityOfficerL2());
          roles.add(new SecurityOfficerL3());
        return roles;
    }
     
   
    
    
}
