/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Customer.Customer;
import Business.EcoSystem;
import Business.Enterprise.BankEnterprise;
import Business.Enterprise.Enterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import userinterface.Customer.FirstTimeLoginAreaJPanel;
import javax.swing.JPanel;
import userinterface.BankManagerRole.CustomerCreatinAreaJPanel;
import userinterface.BankManagerRole.CustomerRelationshipManagerAreaJPanel;


/**
 *
 * @author raunak
 */
public class CustomerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new CustomerRelationshipManagerAreaJPanel(userProcessContainer, account, (BankOrganization)organization,(BankEnterprise) enterprise);
    }
    
}
