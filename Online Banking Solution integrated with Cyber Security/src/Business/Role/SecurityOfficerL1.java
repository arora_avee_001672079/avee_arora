/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Customer.Customer;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import Business.Organization.SecurityOrganisation;
import Business.UserAccount.UserAccount;
import userinterface.Customer.FirstTimeLoginAreaJPanel;
import javax.swing.JPanel;
import userinterface.SecurityOfficer.L1SecurityRelationshipManagerAreaJPanel;
//import UserInterface.SecurityOfficer.L1SecurityRelationshipManagerAreaJPanel;

/**
 *
 * @author raunak
 */
public class SecurityOfficerL1 extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new L1SecurityRelationshipManagerAreaJPanel(userProcessContainer, account, (SecurityOrganisation)organization, enterprise);
    }
    
}
