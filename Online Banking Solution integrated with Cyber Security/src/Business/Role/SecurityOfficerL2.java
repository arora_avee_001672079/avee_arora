/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

/**
 *
 * @author Avee Arora
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import Business.Customer.Customer;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import Business.Organization.SecurityOrganisation;
import Business.UserAccount.UserAccount;
import userinterface.Customer.FirstTimeLoginAreaJPanel;
import javax.swing.JPanel;
import userinterface.SecurityOfficer.L1SecurityRelationshipManagerAreaJPanel;
import userinterface.SecurityOfficer.L2SecurityOfficerManagerWorkAreaJPanel;
//import userInterface.SecurityOfficer.L1SecurityRelationshipManagerAreaJPanel;

/**
 *
 * @author raunak
 */
public class SecurityOfficerL2 extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new L2SecurityOfficerManagerWorkAreaJPanel(userProcessContainer, account, (SecurityOrganisation)organization, enterprise);
    }
    
}
