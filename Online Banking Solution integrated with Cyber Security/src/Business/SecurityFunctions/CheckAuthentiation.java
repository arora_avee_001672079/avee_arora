/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SecurityFunctions;

import Business.Customer.Customer;
import Business.Customer.MasterTransaction;
import Business.Enterprise.Enterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import Business.Organization.SecurityOrganisation;
import Business.SecurityOffice.SecurityConcern;

/**
 *
 * @author Avee Arora
 */
public  class CheckAuthentiation {
    
    private Enterprise enterprise;
    public static void checkIncorrectLoginTimes(Customer customer,Enterprise enterpise)
    {
     
        int currentIncorrectAttempt=customer.getIncorrectLoginTry();
        
        
        if(currentIncorrectAttempt>=3)
        {
            customer.setAccountStatus("Disabled");
            
             Organization org = null;
        for (Organization organizatio : enterpise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                org = organizatio;
                break;
            }
        }
        if (org!=null){
     SecurityConcern concern = org.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Invalid Login Account Locked");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     
     
        }
  
            
        }
    }
    
}
