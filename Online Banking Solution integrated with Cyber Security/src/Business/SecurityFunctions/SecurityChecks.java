/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SecurityFunctions;

import Business.Customer.Beneficiary;
import Business.Customer.Customer;
import Business.Customer.CustomerBankDetails;
import Business.Customer.CustomerTransaction;
import Business.Customer.MasterTransaction;
import Business.Enterprise.BankEnterprise;
import Business.Enterprise.Enterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import Business.Organization.SecurityOrganisation;
import Business.SecurityOffice.SecurityConcern;
import Business.Utilities.AssignRequest;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public  class SecurityChecks {
    
    private Enterprise enterprise;
    public static void checkIncorrectLoginTimes(Customer customer,Enterprise enterpise)
    {
     
        int currentIncorrectAttempt=customer.getIncorrectLoginTry();
        
        
        if(currentIncorrectAttempt>=3)
        {
            customer.setAccountStatus("Disabled");
            
             Organization org = null;
        for (Organization organizatio : enterpise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                org = organizatio;
                break;
            }
        }
        if (org!=null){
     SecurityConcern concern = org.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Invalid Login Account Locked");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
  
            
        }
    }
    
    
    public static void IPCheck(Customer customer,Enterprise enterpise)
    {
         Organization org = null;
        for (Organization organizatio : enterpise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                org = organizatio;
                break;
            }
        }
        if (org!=null){
     SecurityConcern concern = org.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Login From Different Location");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//        String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
    }
    
}
    
    public static void LoginInDay(Customer customer,Enterprise enterpise)
    {
//        Date d = new Date();
//        customer.setLastLogin(d);
//        customer.setLoginInDay(0);
//        
        Date d1 = new Date();
        Date currentTime=d1;
        Date lastLogin=customer.getLastLogin();
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastLogin);
        long lastlogin =cal.getTimeInMillis();
        cal.setTime(currentTime);
        long current=cal.getTimeInMillis();
        long difference=(current-lastlogin)/(60*60*1000);
        if(difference>=24)
        {
            customer.setLoginInDay(1);
        }
        
        int NumberOfTimes=customer.getLoginInDay();
            
           NumberOfTimes=customer.getLoginInDay()+1;
           customer.setLoginInDay(NumberOfTimes);
        System.out.println(NumberOfTimes);
        
       if(customer.getLoginInDay()>=3)
        {
            
            
             Organization org = null;
        for (Organization organizatio : enterpise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                org = organizatio;
                break;
            }
        }
        if (org!=null){
          
            System.out.println("number of times");
     SecurityConcern concern = org.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("More than 3 times Login In a Day");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
  
            
        }
    }
   
   public static void LoginAfter30Days(Customer customer,Enterprise enterpise)
    {
//        Date d = new Date();
//        customer.setLastLogin(d);
//        customer.setLoginInDay(0);
//        
        Date d1 = new Date();
        Date currentTime=d1;
        Date lastLogin=customer.getLastLogin();
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastLogin);
        long lastlogin =cal.getTimeInMillis();
        cal.setTime(currentTime);
        long current=cal.getTimeInMillis();
        long difference=(current-lastlogin)/(60*60*1000*24);
        
       if(difference>=30)
        {
            
            
             Organization org = null;
        for (Organization organizatio : enterpise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                org = organizatio;
                break;
            }
        }
        if (org!=null){
            
     SecurityConcern concern = org.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Login After 30 Days");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
  
            
        }
    }
   
   public static void BeneficiaryInDay(BankEnterprise enterprise,BankOrganization org,CustomerBankDetails customerBankDetails,Customer customer)
   {
       Date d = new Date();
       int count=0;
       for(Beneficiary beneficiary:customerBankDetails.getBeneficiaryList().getBeneficiaryList())
       {
           Date timeCheck=beneficiary.getTimeStamp();
           Calendar cal=Calendar.getInstance();
           cal.setTime(timeCheck);
           long TimeinMili=cal.getTimeInMillis();
           cal.setTime(d);
           long currenTime=cal.getTimeInMillis();
           long diff=(currenTime-TimeinMili)/(60*60*1000);
           if(diff<24)
           {
               count++;
           }
           
       }
       if(count>3)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("Beneficiary");
                System.out.println(customer.getAccountNumber());
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Added More than 3 Beneficiary in a Day");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
   
   public static void OTPLogin(BankEnterprise enterprise,BankOrganization org,Customer customer,Beneficiary beneficiary)
   
   {
      
       beneficiary.setOTPWrong(beneficiary.getOTPWrong()+1);
       int count= beneficiary.getOTPWrong();
     
       
       if(count>3)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("Beneficiary OTP");
                System.out.println(customer.getAccountNumber());
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customer.getAccountNumber());
     concern.setCustomerFirstName(customer.getFirstName());
     concern.setCustomerLastName(customer.getLastName());
     concern.setDescription("Wrong OTP for 3 time on Beneficiary Activation");
     concern.setEmail(customer.getEmail());
     concern.setPhoneNumber(customer.getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
        }
       }
   }
   
   public static void TranscationCheck(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails,float Amount)
   
   {
       
       float maxAmount=0;
      for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
      {
          if(transaction.getTransactionAmount()>maxAmount)
          {
              maxAmount=transaction.getTransactionAmount();
          }
      }
       System.out.println(maxAmount);
    float LimitAmount=maxAmount*3;
        System.out.println("Limit"+LimitAmount);
       if(Amount>LimitAmount)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("Max Limit");
                System.out.println(customer.getAccountNumber());
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("Customer Transaction more than usual and is for amount" +Amount);
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
    

   
   
 public static void newBeneficiaryFundTransferCheck(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails,float Amount,Beneficiary beneficiary)
   
   {
       
       Date d = new Date();
       float totalforDay=0;
       Date timeCheck=beneficiary.getTimeStamp();
           Calendar cal=Calendar.getInstance();
           cal.setTime(timeCheck);
           long TimeinMili=cal.getTimeInMillis();
           cal.setTime(d);
           long currenTime=cal.getTimeInMillis();
           long diff=(currenTime-TimeinMili)/(60*60*1000);
           if(diff<24)
           {
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  
System.out.println(transaction.getTransactionTo().length());
if(transaction.getTransactionTo().length()>0 && transaction.getTransactionTo().equals(beneficiary.getEmail()))
                   {
                   Date transactionCheck=transaction.getTransactionTime();
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000);
           if(diff1<24)
           {
               totalforDay=totalforDay+transaction.getTransactionAmount();
           }
                   }
                   
               }
           }
       totalforDay=totalforDay+Amount;
       float balance=customerBankDetails.getCheckingAccountBalance();
       
       if(totalforDay>10000)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("Beneficiary Transfer");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("Fund Transfer To newly Added Beneficiary more than 20000$");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
  
 public static void allFundTransferCheck(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails,float Amount)
   
   {
       
       Date d = new Date();
       float totalforDay=0;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("Fund Transfer"))
                   {
                   Date transactionCheck=transaction.getTransactionTime();
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000);
           if(diff1<24)
           {
               totalforDay=totalforDay+transaction.getTransactionAmount();
           }
                   }
                   
               }
           
       totalforDay=totalforDay+Amount;
       System.out.println(totalforDay);
       float balance=customerBankDetails.getCheckingAccountBalance();
       
       if(totalforDay>50000)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("Transfer Limit");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("Fund Transfer Limit for the Day Exceded");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
  
 
 
  public static void moreThanFiveTransfer(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails)
   
   {
       
       Date d = new Date();
       float totalforDay=0;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("Fund Transfer"))
                   {
                   Date transactionCheck=transaction.getTransactionTime();
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000);
           if(diff1<24)
           {
               totalforDay=totalforDay+1;
           }
                   }
                   
               }
           
       
       
       if(totalforDay>4)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("More Than 5 transfer");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("More Than 5 Transfer in a Day");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
  
  public static void PreMatureFD(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails)
   
   {
       
       Date d = new Date();
       float totalforDay=0;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("FixedDepositLiquidate"))
                   {
                       System.out.println(transaction.getTransactionAmount());
                   Date transactionCheck=transaction.getTransactionTime();
                       System.out.println(transactionCheck);
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000);
           if(diff1<24)
           {
               totalforDay=totalforDay+1;
           }
                   }
                   
               }
           
       
       
       if(totalforDay>2)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("More Than 3 FD");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("More Than 3 Fixed Deposit Liquidate in a Day");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
   
   
  
 public static void CapitalMarket1stLogin(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails)
   
   {
       
       Date d = new Date();
       boolean flag=false;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("CapitalMarket SELL") ||transaction.getTransactionType().equals("CapitalMarket BUY"))
                   {
                      flag=true;
                      break;
                   }

                   
               }
           
       
       
       if(!flag)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("1st Time Equity");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("1st Time Equity Transcation");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
    
   public static void CapitalMarketTransactionLimit(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails)
   
   {
       
       Date d = new Date();
       float totalforDay=0;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("CapitalMarket SELL") ||transaction.getTransactionType().equals("CapitalMarket BUY"))
                   {
                       System.out.println(transaction.getTransactionAmount());
                   Date transactionCheck=transaction.getTransactionTime();
                       System.out.println(transactionCheck);
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000);
           if(diff1<24)
           {
               totalforDay=totalforDay+1;
           }
                   }
                   
               }
           
       
       
       if(totalforDay>4)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
                System.out.println("More Than 5 Equity");
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("More Than 5 Capital Market Transaction in a Day");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
 
   
public static void CapitalMarketLoginAfter30Days(BankEnterprise enterprise,BankOrganization org,Customer customer,CustomerBankDetails customerBankDetails)
   
   {
       
       Date d = new Date();
       boolean flag=false ;
       
               for(CustomerTransaction transaction:customerBankDetails.getCustomerTransactionList().getCustomerTransactions())
               {
                  

if(transaction.getTransactionType().equals("CapitalMarket SELL") ||transaction.getTransactionType().equals("CapitalMarket BUY"))
                   {
                       System.out.println(transaction.getTransactionAmount());
                   Date transactionCheck=transaction.getTransactionTime();
                       System.out.println(transactionCheck);
                   Calendar cal1=Calendar.getInstance();
           cal1.setTime(transactionCheck);
           long TimeinMili1=cal1.getTimeInMillis();
           cal1.setTime(d);
           long currenTime1=cal1.getTimeInMillis();
           long diff1=(currenTime1-TimeinMili1)/(60*60*1000*24);
           if(diff1<30)
           {
            flag=true;
            break;
           }
                   }
                   
               }
           
       
       
       if(!flag)
       {
           
             Organization organization = null;
        for (Organization organizatio : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organizatio instanceof SecurityOrganisation){
                organization = organizatio;
                break;
            }
        }
        if (organization!=null){
               
                
     SecurityConcern concern = organization.getSecurityConcernsList().addConcern();
     concern.setAccountNumber(customerBankDetails.getCustomer().getAccountNumber());
     concern.setCustomerFirstName(customerBankDetails.getCustomer().getFirstName());
     concern.setCustomerLastName(customerBankDetails.getCustomer().getLastName());
     concern.setDescription("Login After 30 Days in Capital Market Transaction");
     concern.setEmail(customerBankDetails.getCustomer().getEmail());
     concern.setPhoneNumber(customerBankDetails.getCustomer().getPhoneNumber());
     concern.setStatus("Pending");
     concern.setLevel("L1");
     concern.setConcernTimeStamp(new Date());
//     String assignedTo=    AssignRequest.assignSecurityRequest(org);
//        concern.setAssignedTo(assignedTo);
     
        }
       }
   }
     
   
    
    
    
}
