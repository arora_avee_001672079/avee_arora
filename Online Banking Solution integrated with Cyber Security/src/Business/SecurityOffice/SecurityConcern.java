/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SecurityOffice;

import java.util.Date;

/**
 *
 * @author Avee Arora
 */
public class SecurityConcern {
    
    private  int ticketNumber;
    private static int count=0;
    private String CustomerFirstName;
    private String CustomerLastName;
    private String Email;
    private long PhoneNumber;
    private int AccountNumber;
    private int ThreatTransactionNumber;
    private String Description;
    private String Status ;
    private String Finding;
    private String L2Finding;
    
    private String AssignedToL2;
      private String AssignedTo;
    private String Level;
    private Date concernTimeStamp;

    public String getAssignedToL2() {
        return AssignedToL2;
    }

    public void setAssignedToL2(String AssignedToL2) {
        this.AssignedToL2 = AssignedToL2;
    }


    public String getL2Finding() {
        return L2Finding;
    }

    public void setL2Finding(String L2Finding) {
        this.L2Finding = L2Finding;
    }
    
    
    
  
    public Date getConcernTimeStamp() {
        return concernTimeStamp;
    }

    public void setConcernTimeStamp(Date concernTimeStamp) {
        this.concernTimeStamp = concernTimeStamp;
    }

    
    
    public String getAssignedTo() {
        return AssignedTo;
    }

    public void setAssignedTo(String AssignedTo) {
        this.AssignedTo = AssignedTo;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String Level) {
        this.Level = Level;
    }
    
    

    public String getFinding() {
        return Finding;
    }

    public void setFinding(String Finding) {
        this.Finding = Finding;
    }
    

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
    
    public SecurityConcern()
    {
        count++;
        ticketNumber=count;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getCustomerFirstName() {
        return CustomerFirstName;
    }

    public void setCustomerFirstName(String CustomerFirstName) {
        this.CustomerFirstName = CustomerFirstName;
    }

    public String getCustomerLastName() {
        return CustomerLastName;
    }

    public void setCustomerLastName(String CustomerLastName) {
        this.CustomerLastName = CustomerLastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public long getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(long PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public int getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(int AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public int getThreatTransactionNumber() {
        return ThreatTransactionNumber;
    }

    public void setThreatTransactionNumber(int ThreatTransactionNumber) {
        this.ThreatTransactionNumber = ThreatTransactionNumber;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
  
    
}
