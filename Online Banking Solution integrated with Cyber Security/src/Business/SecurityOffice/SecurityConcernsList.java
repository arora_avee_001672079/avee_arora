/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SecurityOffice;

import java.util.ArrayList;

/**
 *
 * @author Avee Arora
 */
public class SecurityConcernsList {
    
     private ArrayList<SecurityConcern> securtiyConcernsList;
    
    public SecurityConcernsList()
    {
        securtiyConcernsList= new ArrayList<>();
    }

    public ArrayList<SecurityConcern> getSecurityConcern() {
        return securtiyConcernsList;
    }

    public void setSecurityConcern(ArrayList<SecurityConcern> securtiyConcernsList) {
        this.securtiyConcernsList = securtiyConcernsList;
    }

    
    
     public SecurityConcern addConcern()
    {
        SecurityConcern a = new SecurityConcern();
        securtiyConcernsList.add(a);
        return a;
    }
    
      public SecurityConcern searchTicket(int TransactionID) {
        for (SecurityConcern p : securtiyConcernsList) {
            if (p.getTicketNumber()==TransactionID) {
                return p;   
            }
        }
        return null;
    }
    
}
