/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import Business.WorkQueue.WorkQueue;

/**
 *
 * @author raunak
 */
public class UserAccount {
    
    private String username;
    private String password;
    private Employee employee;
    private String salt;
    private Role role;
    private WorkQueue workQueue;
    private int NumberOfTicketsAssigneed;
    private int NumberOfTicketsResolved;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    
    
    public int getNumberOfTicketsAssigneed() {
        return NumberOfTicketsAssigneed;
    }

    public void setNumberOfTicketsAssigneed(int NumberOfTicketsAssigneed) {
        this.NumberOfTicketsAssigneed = NumberOfTicketsAssigneed;
    }

    public int getNumberOfTicketsResolved() {
        return NumberOfTicketsResolved;
    }

    public void setNumberOfTicketsResolved(int NumberOfTicketsResolved) {
        this.NumberOfTicketsResolved = NumberOfTicketsResolved;
    }
    
    

    public UserAccount() {
        workQueue = new WorkQueue();
    }
    
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    
    
    @Override
    public String toString() {
        return username;
    }
    
    
    
}