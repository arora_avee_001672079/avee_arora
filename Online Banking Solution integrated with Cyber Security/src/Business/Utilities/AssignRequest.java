/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Avee Arora
 */
public class AssignRequest {
    
    private static Comparator<UserAccount> COMPARATOR = new Comparator<UserAccount>() {
        // This is where the sorting happens.
        public int compare(UserAccount o1, UserAccount o2) {
            return (int) (o1.getNumberOfTicketsAssigneed() - o2.getNumberOfTicketsAssigneed());
        }
    };
    
    public static String assignSecurityRequest(Organization organization)
    {
        String username = null;
        Collections.sort(organization.getUserAccountDirectory().getUserAccountList(), Collections.reverseOrder(COMPARATOR));
       for(UserAccount account: organization.getUserAccountDirectory().getUserAccountList())
       {
          if(account.getRole().toString().equals("Business.Role.SecurityOfficerL1"))
          {
             username=account.getUsername();
             
            account.setNumberOfTicketsAssigneed(account.getNumberOfTicketsAssigneed()+1);
             break;
          }
       }
       return username;
    }
    
}
