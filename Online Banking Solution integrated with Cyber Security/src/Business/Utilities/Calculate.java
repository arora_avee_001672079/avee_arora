/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

/**
 *
 * @author Avee Arora
 */
public class Calculate {
    
    public float calculateFixedDepositAmount(float years , float Amount ,float ROI)
            
    {
        float a = (float) (  Math.pow((1+(ROI/(years*100))), (4*years)));
        return Amount*a;
    }
    
    public float calculateLoanInstallment(float Principle , float ROI , int months)
    {
        float rate=ROI/1200;
        float installment = (float) ((rate+rate/((Math.pow(1+rate,months))-1)) * Principle);
        return installment;
    }
    
    public static float calcluateStockValue(float EPS,float growth)
    {
        float value= (float) ((EPS*7*1.5*growth*4.4)/6.05);
        return value;
    }
    
    public static String calculateMonth(String month)
    {
        String Month=null;
        if(month.equals("01"))
        {
            Month="January";
            
        }
        else if(month.equals("02"))
        {
            Month="Febuary";
            
        }
         else if(month.equals("03"))
        {
            Month="March";
            
        }
          else  if(month.equals("04"))
        {
            Month="April";
            
        }
             else  if(month.equals("05"))
        {
            Month="May";
            
        }
                else     if(month.equals("06"))
        {
            Month="June";
            
        }
               else     if(month.equals("07"))
        {
            Month="July";
            
        }
               else     if(month.equals("08"))
        {
            Month="August";
            
        }
                else     if(month.equals("09"))
        {
            Month="September";
            
        }
               else     if(month.equals("10"))
        {
            Month="October";
            
        }
               else     if(month.equals("11"))
        {
            Month="November";
            
        }
              else     if(month.equals("12"))
        {
            Month="December";
            
        }
        return Month;
    }
    
}
