/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

import Business.Customer.CapitalCompanyData;
import Business.Organization.BankOrganization;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Avee Arora
 */
public class CompanySymbols {
    
    
    private BankOrganization organization;
    public CompanySymbols(BankOrganization organization)
    {
        this.organization=organization;
    }
    public void getCompanySymbols() {

	String csvFile = "C:\\Users\\Avee Arora\\Downloads\\companylist.csv";
	       BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";

	try {

		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {

		        // use comma as separator
			String[] company = line.split(cvsSplitBy);
                         
                        CapitalCompanyData s= organization.getCompanyList().addCompany();
                        s.setCompanyName(company[1]);
                        s.setCompanyCode(company[0]);
                        
			//System.out.println("Country [code= " + country[4] 
                                // + " , name=" + country[5] + "]");

		}

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	System.out.println("Done");
  }
    
}
