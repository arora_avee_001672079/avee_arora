/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

import java.util.Random;

/**
 *
 * @author Avee Arora
 */
public class GenerateUniqueNumberClass {
    
    private static int count=10000;
    private static int fixedDeposit=1000;
    private static int fundTransfer=1000;
    private static int capitalTransaction=10000;
    private static int creditCardTransactionNumber=100;
    
    
      public static int creditCardTransaction()
    {
       creditCardTransactionNumber++;
       return creditCardTransactionNumber;
    }
      public int GenerateCapitalTransaction()
    {
       capitalTransaction++;
       return capitalTransaction;
    }
    
    public int GenerateCustomerOTP()
    {
        Random r = new Random();
        int number=r.nextInt(9999);
        while(number<=999)
        {
        number=r.nextInt(9999);
        }
        return number;
    }
    
    public int GenerateTransaction()
    {
       count++;
       return count;
    }
     public int GenerateFixedDepositNumber()
    {
       fixedDeposit++;
       return fixedDeposit;
    }
       public int GenerateFundTransferNumber()
    {
       fundTransfer++;
       return fundTransfer;
    }
       
       public long GenerateCreditCardNumber()
       {
           Random rng = new Random();
           long first14 = (rng.nextLong() % 100000000000000L) + 5200000000000000L;
           return first14;
       }
     public static int GenerateCvv()
    {
        Random r = new Random();
        int number=r.nextInt(999);
        while(number<=99)
        {
        number=r.nextInt(999);
        }
        return number;
    }
     
     public static String generateCaptcha() {
        Random random = new Random();
        int length = 5 + (Math.abs(random.nextInt()) % 3);
        StringBuffer captchaStrBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int baseCharacterNumber = Math.abs(random.nextInt()) % 62;
            int characterNumber = 0;
            if (baseCharacterNumber < 26) {
                characterNumber = 65 + baseCharacterNumber;
            } else if (baseCharacterNumber < 52) {
                characterNumber = 97 + (baseCharacterNumber - 26);
            } else {
                characterNumber = 48 + (baseCharacterNumber - 52);
            }
            captchaStrBuffer.append((char) characterNumber);
        }
        return captchaStrBuffer.toString();
    }

    
    
}
