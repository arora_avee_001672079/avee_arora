/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.BankManagerRole;

import Business.Customer.Customer;
import Business.Customer.CustomerBankDetails;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Utilities.Mail;
import Business.Organization.BankOrganization;
import Business.Organization.SecurityOrganisation;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import Business.Utilities.GenerateUniqueNumberClass;
import Business.Utils.MyEmailVerifier;
import Business.Utils.MyNoWhiteSpaceVerifier;
import Business.Utils.MyPhoneNumberVerifier;
import Business.Utils.MyStringVerifier;
import Business.WorkQueue.LabTestWorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class CustomerCreatinAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
   private BankOrganization organization;
   private String filename;
   private String uploadfilename;
   String captcha = GenerateUniqueNumberClass.generateCaptcha();
    /**
     * Creates new form RequestLabTestJPanel
     */
    public CustomerCreatinAreaJPanel(JPanel userProcessContainer, BankOrganization organization) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.organization=organization;
        populateRoleComboBox(organization);
         
         showCaptchTxtField.setText(captcha);
         
           MyStringVerifier myStringVerifier = new MyStringVerifier();
        MyPhoneNumberVerifier myPhoneNumberVerifier = new MyPhoneNumberVerifier();
        MyEmailVerifier myEmailVerifier = new MyEmailVerifier();
        MyNoWhiteSpaceVerifier myNoWhiteSpaceVerifier = new MyNoWhiteSpaceVerifier();
        txtAddress.setInputVerifier(myStringVerifier);
        txtPhone.setInputVerifier(myPhoneNumberVerifier);
        txtEmail.setInputVerifier(myEmailVerifier);
        txtFirstName.setInputVerifier(myNoWhiteSpaceVerifier);
        txtLastName.setInputVerifier(myNoWhiteSpaceVerifier);
        txtSSN.setInputVerifier(myPhoneNumberVerifier);
        
         
        //valueLabel.setText(enterprise.getName());
    }

    
     private void populateRoleComboBox(Organization organization){
        roleJComboBox.removeAllItems();
        for (Role role : organization.getSupportedRole()){
        
          
            roleJComboBox.addItem(role);
        }
    }
     
     
      public static BufferedImage scaleImage(int w, int h, BufferedImage img) throws Exception {
    BufferedImage bi;
    bi = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
          Graphics2D g2d = (Graphics2D) bi.createGraphics();
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
    g2d.drawImage(img, 0, 0, w, h, null);
    g2d.dispose();
    return bi;
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        txtPhone = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtLastName = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        txtSSN = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        roleJComboBox = new javax.swing.JComboBox();
        btnBack = new javax.swing.JButton();
        lbImage = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        showCaptchTxtField = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtCaptcha = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setText("First Name");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(305, 133, -1, -1));
        jPanel1.add(txtFirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(431, 131, 126, -1));
        jPanel1.add(txtPhone, new org.netbeans.lib.awtextra.AbsoluteConstraints(435, 243, 126, -1));

        jLabel7.setText("Phone Number");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 243, -1, -1));
        jPanel1.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(435, 203, 126, -1));

        jLabel8.setText("Email");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(305, 203, -1, -1));
        jPanel1.add(txtLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(431, 162, 126, -1));

        jLabel9.setText("Last Name");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(305, 163, -1, -1));

        jLabel10.setText("Address");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 293, -1, -1));
        jPanel1.add(txtAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(435, 293, 126, -1));
        jPanel1.add(txtSSN, new org.netbeans.lib.awtextra.AbsoluteConstraints(437, 339, 126, -1));

        jLabel11.setText("SSN");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 333, -1, -1));

        jLabel1.setText("New Customer");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(389, 28, 205, 42));

        jButton1.setText("Create Customer");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 480, -1, -1));

        jLabel4.setText("Role");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 380, -1, -1));

        roleJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(roleJComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 380, -1, -1));

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        jPanel1.add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 430, -1, -1));

        lbImage.setBackground(new java.awt.Color(102, 102, 102));
        lbImage.setForeground(new java.awt.Color(153, 153, 153));
        jPanel1.add(lbImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 90, 180, 150));

        jButton2.setText("Upload Sign");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 280, -1, -1));

        showCaptchTxtField.setEditable(false);
        showCaptchTxtField.setBackground(new java.awt.Color(223, 223, 223));
        showCaptchTxtField.setFont(new java.awt.Font("Comic Sans MS", 3, 24)); // NOI18N
        showCaptchTxtField.setForeground(new java.awt.Color(255, 51, 51));
        showCaptchTxtField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(showCaptchTxtField, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 420, 100, -1));

        jLabel17.setText("(Case Sensitive)");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 470, -1, -1));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/Customer/retry.png"))); // NOI18N
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });
        jLabel16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel16KeyPressed(evt);
            }
        });
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 430, -1, -1));
        jPanel1.add(txtCaptcha, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 430, 180, -1));

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 990, 540));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
try{
    if(captcha.equals(txtCaptcha.getText()))
    {
        String name = txtFirstName.getText();
        String LastName=txtLastName.getText();
        long Phone = Long.valueOf(txtPhone.getText());
        String Email = txtEmail.getText();
        String address=txtAddress.getText();
        int SSN=Integer.valueOf(txtSSN.getText());

        Customer customer= organization.getCustomerDirectory().createCustomerAccount();
                customer.setAddress(address);
                customer.setEmail(Email);
                customer.setFirstName(name);
                customer.setFirstTimeLogin(true);
                customer.setLastName(LastName);
              Role role = (Role) roleJComboBox.getSelectedItem();
              customer.setRole(role);
                Random r = new Random();
                customer.setUserName(UUID.randomUUID().toString());
                customer.setPassword(UUID.randomUUID().toString());
                customer.setSSN(SSN);
                customer.setOTP(r.nextInt(9999));
                customer.setPhoneNumber(Phone);
                customer.setIncorrectLoginTry(0);
                customer.setAccountStatus("Active");
               String outputFileName="src/Sign/"+customer.getAccountNumber()+".jpg";
        BufferedImage bi;
        try {
            bi = ImageIO.read(new File(uploadfilename));
             File outputfile = new File(outputFileName);
ImageIO.write(bi, "jpg", outputfile);
                
        } catch (IOException ex) {
            Logger.getLogger(CustomerCreatinAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
       
                customer.setSignPath(outputFileName);
        
                Mail m = new Mail();
                m.sendMail("Hello, \n \n welcome to Saving for Life Bank . \n \n Please find below your login Details . \n \n The one time  Username is = "+customer.getUserName()+" . \n \n The one time Password is = "+customer.getPassword(), Email,"New User Credentials" );
        CustomerBankDetails customerBankDetails = organization.getCustomerBankDetails().addBankDetails();
        customerBankDetails.setCustomer(customer);
        customerBankDetails.setCheckingAccountBalance(0);
        customerBankDetails.setCreditCardBalance(0);
        customerBankDetails.setCreditCardLimt(0);
        customerBankDetails.setFixedAccountBalance(0);
        customerBankDetails.setStatus("Activated");
         JOptionPane.showMessageDialog(null, "Customer Created","Warning",JOptionPane.WARNING_MESSAGE);
     
    }
    else
    {
        JOptionPane.showMessageDialog(null, "Please enter correct Captcha ","Warning",JOptionPane.WARNING_MESSAGE);
     
    }
}
catch(Exception e)
{
     JOptionPane.showMessageDialog(null, "Please enter all the values and upload the signature ","Warning",JOptionPane.WARNING_MESSAGE);
       
}
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
//        String path = new File("src/Business/background.jpg")
//                                                           .getAbsolutePath();
         
       //System.out.println(path);
        JFileChooser chooser = new JFileChooser();
    chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
   uploadfilename = f.getAbsolutePath();
   
    try {
        ImageIcon ii=new ImageIcon(scaleImage(120, 120, ImageIO.read(new File(uploadfilename))));//get the image from file chooser and scale it to match JLabel size
        lbImage.setIcon(ii);
        
    } catch (Exception ex) {
        ex.printStackTrace();
    }

        
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        captcha = GenerateUniqueNumberClass.generateCaptcha();
        showCaptchTxtField.setText(captcha);
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel16KeyPressed

    }//GEN-LAST:event_jLabel16KeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbImage;
    private javax.swing.JComboBox roleJComboBox;
    private javax.swing.JTextField showCaptchTxtField;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtCaptcha;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtSSN;
    // End of variables declaration//GEN-END:variables
}
