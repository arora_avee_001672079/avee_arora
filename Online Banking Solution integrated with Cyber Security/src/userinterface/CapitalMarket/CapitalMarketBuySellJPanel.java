/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.CapitalMarket;

import Business.Customer.CapitalCompanyData;
import Business.Customer.Customer;
import Business.Customer.CustomerBankDetails;
import Business.Enterprise.BankEnterprise;
import Business.Organization.BankOrganization;
import Business.Utilities.GetQuotes;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Avee Arora
 */
public class CapitalMarketBuySellJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CapitalMarketBuySellJPanel
     */
 private JPanel container;
    private Customer customer;
    private BankOrganization organization;
    private CustomerBankDetails customerBankDetails  ;
    public CapitalCompanyData company;
     private BankEnterprise enterprise;
    //customerBankDetails=FetchCustomerBankDetails();
    public CapitalMarketBuySellJPanel(JPanel container,Customer customer,BankOrganization organization,CustomerBankDetails customerBankDetails, BankEnterprise enterprise) {
        initComponents();
        this.container=container;
        this.customer=customer;
        this.organization=organization;
        this.customerBankDetails=customerBankDetails;
        this.enterprise=enterprise;
         populateCompany();
   
    }
    
   public void  populateCompany()
    {
         DefaultTableModel dtm = (DefaultTableModel) tblCompany.getModel();
        dtm.setRowCount(0);
        for (CapitalCompanyData a : organization.getCompanyList().getCapitalCompanyDataList()) {
            Object[] row = new Object[2];
            row[0] = a;
            row[1] = a.getCompanyCode();
            
            dtm.addRow(row);
    }
    }
   
   private void refreshProductTable(String keyWord) {
        int rowCount = tblCompany.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tblCompany.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

         for (CapitalCompanyData a : organization.getCompanyList().getCapitalCompanyDataList()) {
String upperCase=a.getCompanyName().toUpperCase();

            if (upperCase.contains(keyWord)) {
                Object row[] = new Object[2];
                row[0] = a;
                row[1] = a.getCompanyCode();

                model.addRow(row);

            }
        }

    }
   
      private String[][] data(String symbol) throws IOException, JSONException
    {
        String URL = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20=%20%22"+symbol+"%22%20and%20startDate%20=%20%222015-09-01%22%20and%20endDate%20=%20%222016-04-21%22&format=json&env=http://datatables.org/alltables.env";
            
         JSONObject json;
        
            json = GetQuotes.readJsonFromUrl(URL);
        
        //JSONObject json = readJsonFromUrl("http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20=%20%22YHOO%22%20and%20startDate%20=%20%222016-01-01%22%20and%20endDate%20=%20%222016-04-05%22&format=json&env=http://datatables.org/alltables.env");
    System.out.println(json.toString());
      JSONArray jar = json.getJSONObject("query").getJSONObject("results").getJSONArray("quote");
      System.out.println(jar.length());
//   String pilot = json.getJSONObject("query").getJSONObject("results").getJSONArray("quote").getJSONObject(0).getString("Close");
//  System.out.println(pilot);
   String[][] Product=new String[jar.length()][2];
   for(int i=0;i<jar.length();i++)
   {
     Product[i][1] = json.getJSONObject("query").getJSONObject("results").getJSONArray("quote").getJSONObject(i).getString("Date"); 
    Product[i][0] = json.getJSONObject("query").getJSONObject("results").getJSONArray("quote").getJSONObject(i).getString("Close"); 
  
   }
   return Product;
    }
    
    public void populateGraph(String symbol) throws IOException, JSONException
    {
        
            DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
           
            String[][] Product= data(symbol);
            for(int i=Product.length-1;i>0;i--)
            {
               // String Month=Calculate.calculateMonth(Product[i][1].substring(5, 7));
               dataset.addValue(Float.parseFloat(Product[i][0]),"Time Period", Product[i][1])   ;
                        
            }

            

           // JFreeChart chart = ChartFactory.createPieChart3D("Poftfolio", dataset, true, true, false);
             JFreeChart lineChart = ChartFactory.createLineChart ("6 month Price History","Months","Price in $",dataset,PlotOrientation.VERTICAL, true,true,false);
            ChartPanel chartPanel = new ChartPanel( lineChart );
      

            panel.removeAll();
            panel.add(chartPanel, BorderLayout.CENTER);
            panel.validate();
    }

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        txtQuote = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCompany = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        panel = new javax.swing.JPanel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("Get Quote");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(302, 244, -1, -1));

        txtQuote.setEnabled(false);
        add(txtQuote, new org.netbeans.lib.awtextra.AbsoluteConstraints(337, 349, 113, -1));

        jLabel1.setText("Price");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(202, 352, -1, -1));

        tblCompany.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Company Name", "Company Code"
            }
        ));
        jScrollPane1.setViewportView(tblCompany);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(108, 60, 464, 173));

        jButton2.setText("Sell");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 450, -1, -1));

        jButton3.setText("Buy");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 450, -1, -1));

        jLabel2.setText("Search Company ");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(158, 23, -1, -1));

        txtSearch.setText(" ");
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(329, 20, 106, -1));

        jButton4.setText("Search");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(727, 19, -1, -1));

        jButton5.setText("Refresh");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 19, -1, -1));

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 421, -1, -1));

        panel.setBackground(new java.awt.Color(204, 204, 204));
        panel.setLayout(new java.awt.BorderLayout());
        add(panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(647, 96, 336, 308));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
try{
        int selectedRow = tblCompany.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first to view detail", "Warning", JOptionPane.WARNING_MESSAGE);

        }else{
             company = (CapitalCompanyData) tblCompany.getValueAt(selectedRow, 0);
            String Code=company.getCompanyCode();

            String URL = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20(%22"+Code+"%22)&format=json&env=http://datatables.org/alltables.env";

            JSONObject json;
            try {
                json = GetQuotes.readJsonFromUrl(URL);
                String pilot = json.getJSONObject("query").getJSONObject("results").getJSONObject("quote").getString("Ask");
                if(pilot.equals("null"))
                {
                 JOptionPane.showMessageDialog(null, "Company has been delisted ","Warning",JOptionPane.WARNING_MESSAGE);
          
                }
                else{
                txtQuote.setText(pilot);
                }
populateGraph(Code);
            } catch (IOException ex) {
                Logger.getLogger(ViewQuotesJPanel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JSONException ex) {
                Logger.getLogger(ViewQuotesJPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            //System.out.println(json.toString());
        }
}
catch(Exception e)
{
    JOptionPane.showMessageDialog(null, "System down for maintance , try after some time", "Warning", JOptionPane.WARNING_MESSAGE);

}

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        float price= Float.parseFloat(txtQuote.getText());
          BuyScriptJPanel panel = new BuyScriptJPanel(container,customer,organization,company,price,customerBankDetails,enterprise);
        container.add("BuyScriptJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
        
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        
           float price= Float.parseFloat(txtQuote.getText());
          SellScriptJPanel panel = new SellScriptJPanel(container,customer,organization,company,price,customerBankDetails,enterprise);
        container.add("BuyScriptJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:

        String keyWord = txtSearch.getText().toUpperCase();
        refreshProductTable(keyWord);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        
        populateCompany();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        container.remove(this);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.previous(container);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    private javax.swing.JTable tblCompany;
    private javax.swing.JTextField txtQuote;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
