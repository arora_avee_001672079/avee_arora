/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.Customer;

import Business.Customer.Customer;
import Business.Customer.CustomerBankDetails;
import Business.Organization.BankOrganization;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Avee Arora
 */
public class CapitalMarketWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CapitalMarketWorkAreaJPanel
     */
    private JPanel container;
    private Customer customer;
    private BankOrganization organization;
    private CustomerBankDetails customerBankDetails  ;
    //customerBankDetails=FetchCustomerBankDetails();
    public CapitalMarketWorkAreaJPanel(JPanel container,Customer customer,BankOrganization organization,CustomerBankDetails customerBankDetails) {
        initComponents();
        this.container=container;
        this.customer=customer;
        this.organization=organization;
         this.customerBankDetails=customerBankDetails;
   
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        jButton1.setText("Buy/Sell");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("View Holdings");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("View Transactions");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("View Portfolio");

        jButton5.setText("View Quotes");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(125, 125, 125)
                .addComponent(jButton4)
                .addGap(41, 41, 41)
                .addComponent(jButton1)
                .addGap(70, 70, 70)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addGap(30, 30, 30)
                .addComponent(jButton3)
                .addGap(101, 101, 101))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(136, 136, 136)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton5))
                .addContainerGap(334, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        
          ViewQuotesJPanel panel = new ViewQuotesJPanel(container,customer,organization ,customerBankDetails);
        container.add("ViewQuotesJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container);  
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
            CapitalMarketBuySellJPanel panel = new CapitalMarketBuySellJPanel(container,customer,organization , customerBankDetails);
        container.add("CapitalMarketBuySellJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        
        CustomerCapitalMarketHoldingJPanel panel = new CustomerCapitalMarketHoldingJPanel(container,customer,organization , customerBankDetails);
        container.add("CustomerCapitalMarketHoldingJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        
         ViewCapitalMarketTransactionsJPanel panel = new ViewCapitalMarketTransactionsJPanel(container,customer,organization , customerBankDetails);
        container.add("ViewCapitalMarketTransactionsJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    // End of variables declaration//GEN-END:variables
}
