/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.Customer;

import userinterface.Loan.CustomerLoanWorkAreaJPanel;
import userinterface.CapitalMarket.CapitalMarketWorkAreaJPanel;
import userinterface.CreditCard.CreditCardworkPlaceJPanel;
import userinterface.FundTransfer.FundTransferWorkAreaJPanel;
import UserInterface.CustomerFD.FixedDepositJPanel;
import Business.Customer.Customer;
import Business.Customer.CustomerBankDetails;
import Business.Customer.CustomerTransaction;
import Business.Enterprise.BankEnterprise;
import Business.Organization.BankOrganization;
import Business.Organization.Organization;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 *
 * @author Avee Arora
 */
public class CustomerWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form CustomerWorkAreaJPanel
     */
     private JPanel container;
    private Customer customer;
    private BankOrganization organization;
    private CustomerBankDetails customerBankDetails  ;
    private BankEnterprise enterprise;
    //customerBankDetails=FetchCustomerBankDetails();
    public CustomerWorkAreaJPanel(JPanel container,Customer customer,BankOrganization organization, BankEnterprise enterprise) {
        initComponents();
        this.container=container;
        this.customer=customer;
        this.organization=organization;
        this.enterprise=enterprise;
         customerBankDetails=FetchCustomerBankDetails();
       txtChecking.setText(String.valueOf(customerBankDetails.getCheckingAccountBalance()));
       txtCaptial.setText(String.valueOf(customerBankDetails.getTotalCapitalMarketInvestment()));
       txtFixed.setText(String.valueOf(customerBankDetails.getFixedAccountBalance()));
       
        
      populateTransction();
      populateGraph();
    }
    

    public CustomerBankDetails FetchCustomerBankDetails()
    {
       CustomerBankDetails customerBankDetails=null;
       for(CustomerBankDetails c :organization.getCustomerBankDetails().getCustomerBankDetails() )
       {
        System.out.println(c.getCustomer().getFirstName());
        if(customer.equals(c.getCustomer()))
                {
                    System.out.println("inside");
                    customerBankDetails=c;
                    break;
                }
       }
       return customerBankDetails;
    }
    
    
    public void populateGraph()
    {
        
            DefaultPieDataset dataset = new DefaultPieDataset();
           
                dataset.setValue("checking Account",customerBankDetails.getCheckingAccountBalance());
                dataset.setValue("Fixed Deposit",customerBankDetails.getFixedAccountBalance());
                        dataset.setValue("Capital Market",customerBankDetails.getTotalCapitalMarketInvestment());
                        
                

            

            JFreeChart chart = ChartFactory.createPieChart3D("Poftfolio", dataset, true, true, false);
            
            PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
            
           

            ChartPanel chartPanel = new ChartPanel(chart);

            panel.removeAll();
            panel.add(chartPanel, BorderLayout.CENTER);
            panel.validate();
    }
    public void populateTransction()
    {
        
        DefaultTableModel dtm = (DefaultTableModel) tblTransaction.getModel();
        dtm.setRowCount(0);
        for (CustomerTransaction transaction : customerBankDetails.getCustomerTransactionList().getCustomerTransactions()) {
            Object[] row = new Object[5];
            row[0] = transaction;
            row[1] = transaction.getMessage();
            row[2] = transaction.getTransactionAmount();
            row[3] = transaction.getStatus();
            row[4] = transaction.getTransactionType();
            
            dtm.addRow(row);
    }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTransaction = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jButton7 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtCaptial = new javax.swing.JTextField();
        txtFixed = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtChecking = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton1.setText("Fixed Deposit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 160, 40));

        jButton3.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton3.setText("Credit Card");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 70, 130, 40));

        jButton4.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton4.setText("Loan");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 90, 40));

        jButton5.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton5.setText("Capital Market");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 70, 190, 40));

        jButton6.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton6.setText("Fund Transfer");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 70, -1, -1));

        tblTransaction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Transaction Time", "Description", "Amount", "Status", "Transaction Type"
            }
        ));
        jScrollPane1.setViewportView(tblTransaction);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 130, 510, 120));

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 472, -1, -1));

        panel.setBackground(new java.awt.Color(204, 204, 204));
        panel.setLayout(new java.awt.BorderLayout());
        add(panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 456, 279));

        jButton7.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jButton7.setText("Payment");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(877, 70, 140, -1));

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel1.setText("Capital Market");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 380, -1, -1));

        txtCaptial.setEnabled(false);
        add(txtCaptial, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 370, 130, -1));

        txtFixed.setEnabled(false);
        add(txtFixed, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 330, 130, -1));

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel2.setText("Fixed Deposit");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 340, -1, -1));

        txtChecking.setEnabled(false);
        add(txtChecking, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 300, 130, -1));

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel3.setText("Checking Account Balance");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 300, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon("C:\\Users\\Avee Arora\\Desktop\\images\\money-40603_960_720.png")); // NOI18N
        jLabel4.setText("jLabel4");
        jLabel4.setMinimumSize(new java.awt.Dimension(300, 200));
        jLabel4.setPreferredSize(new java.awt.Dimension(300, 200));
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 660, 550));

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 2, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 255));
        jLabel5.setText("Account Summary");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 30, 370, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
FixedDepositJPanel panel = new FixedDepositJPanel(container,customer,organization,customerBankDetails,enterprise );
        container.add("FixedDepositJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container);     // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        FundTransferWorkAreaJPanel panel = new FundTransferWorkAreaJPanel(container,customer,organization,customerBankDetails,enterprise );
        container.add("FundTransferWorkAreaJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container);    
        
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
         CreditCardworkPlaceJPanel panel = new CreditCardworkPlaceJPanel(container,customer,organization,customerBankDetails  );
        container.add("CreditCardPaymentJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container);   
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        container.remove(this);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.previous(container);
    }//GEN-LAST:event_btnBackActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        CustomerLoanWorkAreaJPanel panel = new CustomerLoanWorkAreaJPanel(container,customer,organization,customerBankDetails  );
        container.add("CustomerLoanWorkAreaJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
          CapitalMarketWorkAreaJPanel panel = new CapitalMarketWorkAreaJPanel(container,customer,organization,customerBankDetails,enterprise );
        container.add("CapitalMarketWorkAreaJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
        
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
           BillPaymentJPanel panel = new BillPaymentJPanel(container,customer,organization,customerBankDetails );
        container.add("BillPaymentJPanel", panel);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.next(container); 
    }//GEN-LAST:event_jButton7ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    private javax.swing.JTable tblTransaction;
    private javax.swing.JTextField txtCaptial;
    private javax.swing.JTextField txtChecking;
    private javax.swing.JTextField txtFixed;
    // End of variables declaration//GEN-END:variables
}
