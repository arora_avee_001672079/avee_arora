/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.Customer;

import Business.Customer.Customer;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.SecurityOrganisation;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.Utilities.GenerateUniqueNumberClass;
import Business.Utilities.HashUtil;
import Business.Utils.MyEmailVerifier;
import Business.Utils.MyNoWhiteSpaceVerifier;
import Business.Utils.MyPhoneNumberVerifier;
import Business.Utils.MyStringVerifier;
import Business.WorkQueue.LabTestWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author raunak
 */
public class FirstTimeLoginAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userAccount;
    private SecurityOrganisation labOrganization;
    private Customer customer;
    private EcoSystem system;
    String captcha = GenerateUniqueNumberClass.generateCaptcha();
    /**
     * Creates new form LabAssistantWorkAreaJPanel
     */
    public FirstTimeLoginAreaJPanel(JPanel userProcessContainer , Customer customer,EcoSystem system) {
        initComponents();
        
    this.userProcessContainer = userProcessContainer;
      this.customer = customer;
      this.system=system;
      txtAddress.setText(customer.getAddress());
        txtEmail.setText(customer.getEmail());
        
        showCaptchTxtField.setText(captcha);
        
          MyStringVerifier myStringVerifier = new MyStringVerifier();
        MyPhoneNumberVerifier myPhoneNumberVerifier = new MyPhoneNumberVerifier();
        MyEmailVerifier myEmailVerifier = new MyEmailVerifier();
        MyNoWhiteSpaceVerifier myNoWhiteSpaceVerifier = new MyNoWhiteSpaceVerifier();
        txtAddress.setInputVerifier(myStringVerifier);
        txtPhone.setInputVerifier(myPhoneNumberVerifier);
        txtEmail.setInputVerifier(myEmailVerifier);
        txtuserName.setInputVerifier(myNoWhiteSpaceVerifier);
        txtFirstName.setText(customer.getFirstName());
        txtLastName.setText(customer.getLastName());
        txtPhone.setText(String.valueOf(customer.getPhoneNumber()));
        
        //txt.setInputVerifier(myStringVerifier);

//        this.business = business;
//        this.labOrganization = (SecurityOrganisation)organization;
      
        //populateTable();
    }
    
    private boolean checkUserName(String userName)
    {
        boolean flag = false;
            //Step2: Go inside each network to check each enterprise
            for (Network network : system.getNetworkList()) {
                //Step 2-a: Check against each enterprise
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    
                    
                    userAccount = enterprise.getUserAccountDirectory().authenticateUserName(userName);
                    if (userAccount == null) {
                        //Step3: Check against each organization inside that enterprise
                        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                            
                       
                            userAccount = organization.getUserAccountDirectory().authenticateUserName(userName);
                            if (userAccount != null) {
                                flag=true;
                                break;
                            }
                            else
                            {
                                Customer customer = organization.getCustomerDirectory().searchCustomer(userName);
                                if(customer != null)
                                {
                                    flag=true;
                                    break;
                                }
                            }
                        }   
                    }
                    else
                    {
                        flag=true;
                        break;
                    }
                
            }
        }
        return flag;
    }
    
public JXDatePicker calender()
{
     JFrame frame = new JFrame("JXPicker Example");
        JPanel panel = new JPanel();

        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setBounds(400, 400, 250, 100);

        JXDatePicker picker = new JXDatePicker();
        picker.setDate(Calendar.getInstance().getTime());
        picker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));

        panel.add(picker);
        frame.getContentPane().add(panel);
       // txtDOB.setText(picker.getDate().toString());

        frame.setVisible(true);
        return picker;
}

 


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        txtPhone = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtLastName = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        txtuserName = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtPassword = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        dateOfBirth = new org.jdesktop.swingx.JXDatePicker();
        cbQuestion1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbQuestion2 = new javax.swing.JComboBox<>();
        txtAnswer1 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtCaptcha = new javax.swing.JTextField();
        showCaptchTxtField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtAnswer3 = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setText("First Name");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 70, -1, -1));
        add(txtFirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 70, 126, -1));
        add(txtPhone, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 126, -1));

        jLabel7.setText("Phone Number");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, -1, -1));
        add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 140, 126, -1));

        jLabel8.setText("Email");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 150, -1, -1));
        add(txtLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 100, 126, -1));

        jLabel9.setText("Last Name");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, -1, -1));

        jLabel10.setText("Address");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 240, -1, -1));
        add(txtAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, 126, -1));
        add(txtuserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, 126, -1));

        jLabel11.setText("UserName");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, -1, 20));

        jLabel1.setText("New Customer");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 0, 205, 42));

        jButton1.setText("Change Password and Activate Account");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 460, -1, -1));
        add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 380, 126, -1));

        jLabel12.setText("Password");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 380, -1, 20));

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 430, -1, -1));

        jLabel13.setText("DOB");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 300, -1, -1));

        dateOfBirth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateOfBirthActionPerformed(evt);
            }
        });
        add(dateOfBirth, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 300, -1, -1));

        cbQuestion1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "What is the name of your Birth City ?", "Which is your faviourite Sport ?", "Where did you meet your spose first time?" }));
        cbQuestion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbQuestion1ActionPerformed(evt);
            }
        });
        add(cbQuestion1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 80, 210, 20));

        jLabel2.setText("Secuirty Question 1");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 84, 100, 20));

        jLabel3.setText("Secuirty Question 2");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 180, 100, 20));

        cbQuestion2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "What is the name of your pet ?", "Which is the name of your first manager ?", "Where did you did your first job?" }));
        cbQuestion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbQuestion2ActionPerformed(evt);
            }
        });
        add(cbQuestion2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 190, 180, 20));
        add(txtAnswer1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 130, 180, -1));

        jLabel14.setText("Answer 1");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 140, -1, -1));

        jLabel15.setText("Answer 2");
        add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 240, -1, -1));
        add(txtCaptcha, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 360, 180, -1));

        showCaptchTxtField.setEditable(false);
        showCaptchTxtField.setBackground(new java.awt.Color(223, 223, 223));
        showCaptchTxtField.setFont(new java.awt.Font("Comic Sans MS", 3, 24)); // NOI18N
        showCaptchTxtField.setForeground(new java.awt.Color(255, 51, 51));
        showCaptchTxtField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        add(showCaptchTxtField, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 350, 100, -1));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userinterface/Customer/retry.png"))); // NOI18N
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });
        jLabel16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel16KeyPressed(evt);
            }
        });
        add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 360, -1, -1));

        jLabel17.setText("(Case Sensitive)");
        add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 400, -1, -1));

        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
        });
        jLabel18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jLabel18KeyPressed(evt);
            }
        });
        add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
        add(txtAnswer3, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 230, 180, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
        if(captcha.equals(txtCaptcha.getText()))
        {
            boolean check=checkUserName(txtuserName.getText());
            System.out.println("Username"+check);
            if(!check)
            {
                
        String password=txtPassword.getText();
        String salt=null;
        try {
            salt = HashUtil.getSalt(128);
             String passwordHex = HashUtil.getHash(password, salt);
            customer.setPassword(passwordHex);
            customer.setSalt(salt);
        } catch (Exception ex) {
            Logger.getLogger(FirstTimeLoginAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
           
        }
                        
        String q1=cbQuestion1.getSelectedItem().toString();
        String q2=cbQuestion2.getSelectedItem().toString();
       
        customer.setQ1(q1);
        customer.setQ2(q2);
        customer.setDOB(dateOfBirth.getDate().toString());
        customer.setAns1(txtAnswer1.getText());
        customer.setAns2(txtCaptcha.getText());
        customer.setUserName(txtuserName.getText());
        customer.setFirstTimeLogin(false);
        Date d = new Date();
        customer.setLastLogin(d);
        customer.setLoginInDay(0);
        InetAddress IP;
          JOptionPane.showMessageDialog(null, "Account Created , Please Relogin with new credentials ","Warning",JOptionPane.WARNING_MESSAGE);
         

        userProcessContainer.removeAll();
        JPanel blankJP = new JPanel();
        userProcessContainer.add("blank", blankJP);
        CardLayout crdLyt = (CardLayout) userProcessContainer.getLayout();
        crdLyt.next(userProcessContainer);
        
        
        try {
            IP = InetAddress.getLocalHost();
            customer.setIPAddress(IP.getHostAddress());
        } catch (UnknownHostException ex) {
            Logger.getLogger(FirstTimeLoginAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
            else if(check)
            {
              JOptionPane.showMessageDialog(null, "UserName Exists , Please enter different UserName ","Warning",JOptionPane.WARNING_MESSAGE);
           
            }
        }
        else
        {
             JOptionPane.showMessageDialog(null, "Please enter the correct Captcha ","Warning",JOptionPane.WARNING_MESSAGE);
        
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void dateOfBirthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateOfBirthActionPerformed
        // TODO add your handling code here:
        calender();
    }//GEN-LAST:event_dateOfBirthActionPerformed

    private void cbQuestion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbQuestion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbQuestion1ActionPerformed

    private void cbQuestion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbQuestion2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbQuestion2ActionPerformed

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        captcha = GenerateUniqueNumberClass.generateCaptcha();
        showCaptchTxtField.setText(captcha);
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel16KeyPressed

    }//GEN-LAST:event_jLabel16KeyPressed

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
       
    }//GEN-LAST:event_jLabel18MouseClicked

    private void jLabel18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel18KeyPressed

    }//GEN-LAST:event_jLabel18KeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JComboBox<String> cbQuestion1;
    private javax.swing.JComboBox<String> cbQuestion2;
    private org.jdesktop.swingx.JXDatePicker dateOfBirth;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField showCaptchTxtField;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtAnswer1;
    private javax.swing.JTextField txtAnswer3;
    private javax.swing.JTextField txtCaptcha;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtuserName;
    // End of variables declaration//GEN-END:variables
}
