/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.SecurityOfficer;

import Business.Customer.Dummy;
import Business.Customer.DummyList;
import Business.Enterprise.Enterprise;
import Business.Organization.SecurityOrganisation;
import Business.SecurityOffice.SecurityConcern;
import Business.UserAccount.UserAccount;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Avee Arora
 */
public class AnalyticsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AnalyticsJPanel
     */
    private JPanel userProcessContainer;
    private SecurityOrganisation organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    /**
     * Creates new form DoctorWorkAreaJPanel
     */
    public AnalyticsJPanel(JPanel userProcessContainer, UserAccount account, SecurityOrganisation organization, Enterprise enterprise) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        
             
  }
    
    private static Comparator<Dummy> COMPARATOR = new Comparator<Dummy>() {
        // This is where the sorting happens.
        public int compare(Dummy o1, Dummy o2) {
            return (int) (o1.getTotal() - o2.getTotal());
        }
    };
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel.setBackground(new java.awt.Color(204, 204, 204));
        panel.setLayout(new java.awt.BorderLayout());
        add(panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 86, 560, 380));

        jButton1.setForeground(new java.awt.Color(0, 0, 255));
        jButton1.setText("Top 3  L1 security Ticket Solver");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(29, 152, 221, 37));

        jButton2.setText("Top 3 L2 Security Ticket Solver");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 220, 40));

        jButton3.setText("Tickets Raised");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 220, -1));

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 2, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 255));
        jLabel5.setText("Security Data Points");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 30, 370, -1));

        btnBack.setText("<<BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 500, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Avee Arora\\Desktop\\images\\graph-163509_960_720.jpg")); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 930, 550));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        DummyList list = new DummyList();

        int initial = 0;
        int finalTotal = 0;
        boolean flag = false;

        for (SecurityConcern o : organization.getSecurityConcernsList().getSecurityConcern()) {
            
                if(o.getStatus().equals("Closed")||o.getStatus().equals("Assigned to L2"))
                {
                   
                System.out.println("hii thi is "+o.getAssignedTo());
                    for (Dummy d : list.getDummyList()) {

                        if (d.getName().equals(o.getAssignedTo())) {
                            flag = true;

                        } else {
                            flag = false;
                        }

                    }
                    if (flag == false) {
                        Dummy d = list.addOrder();

                       
                        d.setTotal(0);
                        d.setName(o.getAssignedTo());

                    } else if (flag == true) {
                        for (Dummy d : list.getDummyList()) {
                            if (d.getName().equals(o.getAssignedTo())) {
                                finalTotal = (d.getTotal()+1);

                                d.setTotal(finalTotal);
                            }
                        }

                    }
                    
                }
        }

        int count = 0;
        Collections.sort(list.getDummyList(), Collections.reverseOrder(COMPARATOR));
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Dummy dummy : list.getDummyList()) {
            dataset.setValue(dummy.getTotal(), "Top 3 Security Person", dummy.getName());

            count++;
            if (count >= 3) {
                break;
            }

        }

        JFreeChart chart = ChartFactory.createBarChart("Top 3 L2 Security Person", "Security Person Name", "Volume", dataset, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot catPlot = chart.getCategoryPlot();
        catPlot.setRangeGridlinePaint(Color.BLACK);

        ChartPanel chartPanel = new ChartPanel(chart);

        panel.removeAll();
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();

        //        for(Dummy d :list.getDummyList())
        //        {
            //            System.out.println("inside");
            //            System.out.println(d.getId());
            //            System.out.println(d.getTotal());
            //
            //        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        DummyList list = new DummyList();

        int initial = 0;
        int finalTotal = 0;
        boolean flag = false;

        for (SecurityConcern o : organization.getSecurityConcernsList().getSecurityConcern()) {
            
                if(o.getStatus().equals("Closed by L2"))
                {
                   
                
                    for (Dummy d : list.getDummyList()) {

                        if (d.getName().equals(o.getAssignedToL2())) {
                            flag = true;

                        } else {
                            flag = false;
                        }

                    }
                    if (flag == false) {
                        Dummy d = list.addOrder();

                       
                        d.setTotal(0);
                        d.setName(o.getAssignedToL2());

                    } else if (flag == true) {
                        for (Dummy d : list.getDummyList()) {
                            if (d.getName().equals(o.getAssignedToL2())) {
                                finalTotal = (d.getTotal()+1);

                                d.setTotal(finalTotal);
                            }
                        }

                    }
                    
                }
        }

        int count = 0;
        Collections.sort(list.getDummyList(), Collections.reverseOrder(COMPARATOR));
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Dummy dummy : list.getDummyList()) {
            dataset.setValue(dummy.getTotal(), "Top 3 L2 Security Person", dummy.getName());

            count++;
            if (count >= 3) {
                break;
            }

        }

        JFreeChart chart = ChartFactory.createBarChart("Top 3 L2 Security Person", "Security Person Name", "Volume", dataset, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot catPlot = chart.getCategoryPlot();
        catPlot.setRangeGridlinePaint(Color.BLACK);

        ChartPanel chartPanel = new ChartPanel(chart);

        panel.removeAll();
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();

        //        for(Dummy d :list.getDummyList())
        //        {
            //            System.out.println("inside");
            //            System.out.println(d.getId());
            //            System.out.println(d.getTotal());
            //
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        
        DummyList list = new DummyList();

        int initial = 0;
        int finalTotal = 0;
        boolean flag = false;

        for (SecurityConcern o : organization.getSecurityConcernsList().getSecurityConcern()) {
            
               
                   
                
                    for (Dummy d : list.getDummyList()) {

                        if (d.getName().equals(o.getDescription())) {
                            flag = true;

                        } else {
                            flag = false;
                        }

                    }
                    if (flag == false) {
                        Dummy d = list.addOrder();

                       
                        d.setTotal(0);
                        d.setName(o.getDescription());

                    } else if (flag == true) {
                        for (Dummy d : list.getDummyList()) {
                            if (d.getName().equals(o.getDescription())) {
                                finalTotal = (d.getTotal()+1);

                                d.setTotal(finalTotal);
                            }
                        }

                    
                    
                }
        }

        int count = 0;
        Collections.sort(list.getDummyList(), Collections.reverseOrder(COMPARATOR));
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Dummy dummy : list.getDummyList()) {
            dataset.setValue(dummy.getTotal(), "Tickets Ratio", dummy.getName());

            count++;
            if (count >= 10) {
                break;
            }

        }

        JFreeChart chart = ChartFactory.createBarChart("Ticket Ratio", "Type", "Volume", dataset, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot catPlot = chart.getCategoryPlot();
        catPlot.setRangeGridlinePaint(Color.BLACK);

        ChartPanel chartPanel = new ChartPanel(chart);

        panel.removeAll();
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();

        //        for(Dummy d :list.getDummyList())
        //        {
            //            System.out.println("inside");
            //            System.out.println(d.getId());
            //            System.out.println(d.getTotal());
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
